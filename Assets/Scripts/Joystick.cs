﻿using UnityEngine;
using System.Collections;

public class Joystick : MonoBehaviour {

	public float modifier = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//controls the rotation of the joystick
		transform.localRotation = Quaternion.Euler( new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical") ) * modifier * -1);
	}
}
