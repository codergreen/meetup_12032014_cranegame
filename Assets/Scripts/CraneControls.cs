﻿using UnityEngine;
using System.Collections;

public class CraneControls : MonoBehaviour {

	public float speed = 1;
	public float forceMultiplier = 1.0f;

	public GameObject craneMount;
	public GameObject craneControlArmAssembly;

	public Transform leftBounds;//TODO: Add the constraints to the left and right movement
	public Transform rightBounds;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//Left and Right movement of craneMount
		craneMount.transform.Translate( Vector3.back * Input.GetAxis("Horizontal") * speed * Time.deltaTime);
	}

	void FixedUpdate()
	{
		//forward and back movement of craneControlArmAssembly
		craneControlArmAssembly.rigidbody.AddForce( Vector3.forward * Input.GetAxis("Vertical") * forceMultiplier);
	}
}
