//Maya ASCII 2014 scene
//Name: crane_game.ma
//Last modified: Mon, Dec 01, 2014 10:42:04 PM
//Codeset: UTF-8
requires maya "2014";
requires -nodeType "mentalrayFramebuffer" -nodeType "mentalrayOutputPass" -nodeType "mentalrayRenderPass"
		 -nodeType "mentalrayUserBuffer" -nodeType "mentalraySubdivApprox" -nodeType "mentalrayCurveApprox"
		 -nodeType "mentalraySurfaceApprox" -nodeType "mentalrayDisplaceApprox" -nodeType "mentalrayOptions"
		 -nodeType "mentalrayGlobals" -nodeType "mentalrayItemsList" -nodeType "mentalrayShader"
		 -nodeType "mentalrayUserData" -nodeType "mentalrayText" -nodeType "mentalrayTessellation"
		 -nodeType "mentalrayPhenomenon" -nodeType "mentalrayLightProfile" -nodeType "mentalrayVertexColors"
		 -nodeType "mentalrayIblShape" -nodeType "mapVizShape" -nodeType "mentalrayCCMeshProxy"
		 -nodeType "cylindricalLightLocator" -nodeType "discLightLocator" -nodeType "rectangularLightLocator"
		 -nodeType "sphericalLightLocator" -nodeType "abcimport" -nodeType "mia_physicalsun"
		 -nodeType "mia_physicalsky" -nodeType "mia_material" -nodeType "mia_material_x" -nodeType "mia_roundcorners"
		 -nodeType "mia_exposure_simple" -nodeType "mia_portal_light" -nodeType "mia_light_surface"
		 -nodeType "mia_exposure_photographic" -nodeType "mia_exposure_photographic_rev" -nodeType "mia_lens_bokeh"
		 -nodeType "mia_envblur" -nodeType "mia_ciesky" -nodeType "mia_photometric_light"
		 -nodeType "mib_texture_vector" -nodeType "mib_texture_remap" -nodeType "mib_texture_rotate"
		 -nodeType "mib_bump_basis" -nodeType "mib_bump_map" -nodeType "mib_passthrough_bump_map"
		 -nodeType "mib_bump_map2" -nodeType "mib_lookup_spherical" -nodeType "mib_lookup_cube1"
		 -nodeType "mib_lookup_cube6" -nodeType "mib_lookup_background" -nodeType "mib_lookup_cylindrical"
		 -nodeType "mib_texture_lookup" -nodeType "mib_texture_lookup2" -nodeType "mib_texture_filter_lookup"
		 -nodeType "mib_texture_checkerboard" -nodeType "mib_texture_polkadot" -nodeType "mib_texture_polkasphere"
		 -nodeType "mib_texture_turbulence" -nodeType "mib_texture_wave" -nodeType "mib_reflect"
		 -nodeType "mib_refract" -nodeType "mib_transparency" -nodeType "mib_continue" -nodeType "mib_opacity"
		 -nodeType "mib_twosided" -nodeType "mib_refraction_index" -nodeType "mib_dielectric"
		 -nodeType "mib_ray_marcher" -nodeType "mib_illum_lambert" -nodeType "mib_illum_phong"
		 -nodeType "mib_illum_ward" -nodeType "mib_illum_ward_deriv" -nodeType "mib_illum_blinn"
		 -nodeType "mib_illum_cooktorr" -nodeType "mib_illum_hair" -nodeType "mib_volume"
		 -nodeType "mib_color_alpha" -nodeType "mib_color_average" -nodeType "mib_color_intensity"
		 -nodeType "mib_color_interpolate" -nodeType "mib_color_mix" -nodeType "mib_color_spread"
		 -nodeType "mib_geo_cube" -nodeType "mib_geo_torus" -nodeType "mib_geo_sphere" -nodeType "mib_geo_cone"
		 -nodeType "mib_geo_cylinder" -nodeType "mib_geo_square" -nodeType "mib_geo_instance"
		 -nodeType "mib_geo_instance_mlist" -nodeType "mib_geo_add_uv_texsurf" -nodeType "mib_photon_basic"
		 -nodeType "mib_light_infinite" -nodeType "mib_light_point" -nodeType "mib_light_spot"
		 -nodeType "mib_light_photometric" -nodeType "mib_cie_d" -nodeType "mib_blackbody"
		 -nodeType "mib_shadow_transparency" -nodeType "mib_lens_stencil" -nodeType "mib_lens_clamp"
		 -nodeType "mib_lightmap_write" -nodeType "mib_lightmap_sample" -nodeType "mib_amb_occlusion"
		 -nodeType "mib_fast_occlusion" -nodeType "mib_map_get_scalar" -nodeType "mib_map_get_integer"
		 -nodeType "mib_map_get_vector" -nodeType "mib_map_get_color" -nodeType "mib_map_get_transform"
		 -nodeType "mib_map_get_scalar_array" -nodeType "mib_map_get_integer_array" -nodeType "mib_fg_occlusion"
		 -nodeType "mib_bent_normal_env" -nodeType "mib_glossy_reflection" -nodeType "mib_glossy_refraction"
		 -nodeType "builtin_bsdf_architectural" -nodeType "builtin_bsdf_architectural_comp"
		 -nodeType "builtin_bsdf_carpaint" -nodeType "builtin_bsdf_ashikhmin" -nodeType "builtin_bsdf_lambert"
		 -nodeType "builtin_bsdf_mirror" -nodeType "builtin_bsdf_phong" -nodeType "contour_store_function"
		 -nodeType "contour_store_function_simple" -nodeType "contour_contrast_function_levels"
		 -nodeType "contour_contrast_function_simple" -nodeType "contour_shader_simple" -nodeType "contour_shader_silhouette"
		 -nodeType "contour_shader_maxcolor" -nodeType "contour_shader_curvature" -nodeType "contour_shader_factorcolor"
		 -nodeType "contour_shader_depthfade" -nodeType "contour_shader_framefade" -nodeType "contour_shader_layerthinner"
		 -nodeType "contour_shader_widthfromcolor" -nodeType "contour_shader_widthfromlightdir"
		 -nodeType "contour_shader_widthfromlight" -nodeType "contour_shader_combi" -nodeType "contour_only"
		 -nodeType "contour_composite" -nodeType "contour_ps" -nodeType "mi_metallic_paint"
		 -nodeType "mi_metallic_paint_x" -nodeType "mi_bump_flakes" -nodeType "mi_car_paint_phen"
		 -nodeType "mi_metallic_paint_output_mixer" -nodeType "mi_car_paint_phen_x" -nodeType "physical_lens_dof"
		 -nodeType "physical_light" -nodeType "dgs_material" -nodeType "dgs_material_photon"
		 -nodeType "dielectric_material" -nodeType "dielectric_material_photon" -nodeType "oversampling_lens"
		 -nodeType "path_material" -nodeType "parti_volume" -nodeType "parti_volume_photon"
		 -nodeType "transmat" -nodeType "transmat_photon" -nodeType "mip_rayswitch" -nodeType "mip_rayswitch_advanced"
		 -nodeType "mip_rayswitch_environment" -nodeType "mip_card_opacity" -nodeType "mip_motionblur"
		 -nodeType "mip_motion_vector" -nodeType "mip_matteshadow" -nodeType "mip_cameramap"
		 -nodeType "mip_mirrorball" -nodeType "mip_grayball" -nodeType "mip_gamma_gain" -nodeType "mip_render_subset"
		 -nodeType "mip_matteshadow_mtl" -nodeType "mip_binaryproxy" -nodeType "mip_rayswitch_stage"
		 -nodeType "mip_fgshooter" -nodeType "mib_ptex_lookup" -nodeType "misss_physical"
		 -nodeType "misss_physical_phen" -nodeType "misss_fast_shader" -nodeType "misss_fast_shader_x"
		 -nodeType "misss_fast_shader2" -nodeType "misss_fast_shader2_x" -nodeType "misss_skin_specular"
		 -nodeType "misss_lightmap_write" -nodeType "misss_lambert_gamma" -nodeType "misss_call_shader"
		 -nodeType "misss_set_normal" -nodeType "misss_fast_lmap_maya" -nodeType "misss_fast_simple_maya"
		 -nodeType "misss_fast_skin_maya" -nodeType "misss_fast_skin_phen" -nodeType "misss_fast_skin_phen_d"
		 -nodeType "misss_mia_skin2_phen" -nodeType "misss_mia_skin2_phen_d" -nodeType "misss_lightmap_phen"
		 -nodeType "misss_mia_skin2_surface_phen" -nodeType "surfaceSampler" -nodeType "mib_data_bool"
		 -nodeType "mib_data_int" -nodeType "mib_data_scalar" -nodeType "mib_data_vector"
		 -nodeType "mib_data_color" -nodeType "mib_data_string" -nodeType "mib_data_texture"
		 -nodeType "mib_data_shader" -nodeType "mib_data_bool_array" -nodeType "mib_data_int_array"
		 -nodeType "mib_data_scalar_array" -nodeType "mib_data_vector_array" -nodeType "mib_data_color_array"
		 -nodeType "mib_data_string_array" -nodeType "mib_data_texture_array" -nodeType "mib_data_shader_array"
		 -nodeType "mib_data_get_bool" -nodeType "mib_data_get_int" -nodeType "mib_data_get_scalar"
		 -nodeType "mib_data_get_vector" -nodeType "mib_data_get_color" -nodeType "mib_data_get_string"
		 -nodeType "mib_data_get_texture" -nodeType "mib_data_get_shader" -nodeType "mib_data_get_shader_bool"
		 -nodeType "mib_data_get_shader_int" -nodeType "mib_data_get_shader_scalar" -nodeType "mib_data_get_shader_vector"
		 -nodeType "mib_data_get_shader_color" -nodeType "user_ibl_env" -nodeType "user_ibl_rect"
		 -nodeType "mia_material_x_passes" -nodeType "mi_metallic_paint_x_passes" -nodeType "mi_car_paint_phen_x_passes"
		 -nodeType "misss_fast_shader_x_passes" -dataType "byteArray" "Mayatomr" "2014.0 - 3.11.1.13 ";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2014";
fileInfo "version" "2014";
fileInfo "cutIdentifier" "201310090405-890429";
fileInfo "osv" "Mac OS X 10.9.1";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 32.271901646171415 11.266825120544787 0.88648008138664913 ;
	setAttr ".r" -type "double3" -12.600016428682608 445.1999997970122 9.5023796641254361e-15 ;
	setAttr ".rp" -type "double3" 0 0 -1.1102230246251565e-16 ;
	setAttr ".rpt" -type "double3" -3.4476342391023863e-15 3.1235837273299132e-15 -9.7376463671261932e-17 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999979;
	setAttr ".coi" 34.131190242414768;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" -0.92047682404518127 3.8213270970758724 -1.9007619321346287 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 100.1 0 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.1 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 100.1 0 0 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 100.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "crane_game";
	setAttr ".rp" -type "double3" 0.70417574085243029 7.2088550935925344 0 ;
	setAttr ".sp" -type "double3" 0.70417574085243029 7.2088550935925344 0 ;
createNode transform -n "control_arm_stabilizer_right" -p "crane_game";
	setAttr ".t" -type "double3" 0.017294300097894677 13.759995607685143 -4.2489608738891587 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1 1 0.93388309614860188 ;
createNode mesh -n "control_arm_stabilizer_rightShape" -p "control_arm_stabilizer_right";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.11502507 -0.14601529 4.32033253 0.11502507 -0.14601529 4.32033253
		 -0.11502507 0.14601529 4.32033253 0.11502507 0.14601529 4.32033253 -0.11502507 0.14601529 -4.32033253
		 0.11502507 0.14601529 -4.32033253 -0.11502507 -0.14601529 -4.32033253 0.11502507 -0.14601529 -4.32033253;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "control_arm_stabilizer_left" -p "crane_game";
	setAttr ".t" -type "double3" 0.017294300097894677 13.759995607685143 4.3806772908391327 ;
	setAttr ".r" -type "double3" 0 90 0 ;
	setAttr ".s" -type "double3" 1 1 0.93388309614860188 ;
createNode mesh -n "control_arm_stabilizer_leftShape" -p "control_arm_stabilizer_left";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.11502507 -0.14601529 4.32033253 0.11502507 -0.14601529 4.32033253
		 -0.11502507 0.14601529 4.32033253 0.11502507 0.14601529 4.32033253 -0.11502507 0.14601529 -4.32033253
		 0.11502507 0.14601529 -4.32033253 -0.11502507 -0.14601529 -4.32033253 0.11502507 -0.14601529 -4.32033253;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crane_prize_base" -p "crane_game";
	setAttr ".t" -type "double3" -0.6055668433069652 3.799899254481133 -0.0077842272198894724 ;
	setAttr ".s" -type "double3" 0.83389485267788577 1 1 ;
createNode mesh -n "crane_prize_baseShape" -p "crane_prize_base";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 46 ".uvst[0].uvsp[0:45]" -type "float2" 0 0 0.83532989 0
		 0 1 0.83532989 1 0 0 0.83532989 0 0.83532989 1 0 1 0 0 0.83532989 0 0.83532989 1
		 0 1 0.79635382 0 0.79635382 1 0.79635382 1 0.79635382 1 0.79635382 0 0.79635382 0
		 0.83532989 0.2592766 0.79635382 0.2592766 0 0.2592766 0 0.2592766 0 0.2592766 0.79635382
		 0.25927663 0.83532989 0.2592766 0.83532989 0.2592766 0.83532989 0.2592766 0.83532989
		 0.2592766 0.83532989 1 0.83532989 1 0.79635382 0 0.79635382 0.25927663 0.83532989
		 0.2592766 0.83532989 0 0.83532989 0 0.83532989 0.2592766 0.79635382 0.25927663 0.79635382
		 0 0.83532989 0.2592766 0.83532989 0 0.83532989 0 0.83532989 0.2592766 0 0 1 0 1 1
		 0 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 34 ".vt[0:33]"  -4.097939968 0 4.81977272 3.033229828 0 4.81977272
		 -4.097939968 0 -4.73569536 3.033229589 0 -4.73569536 -4.097939968 0.22395825 4.81977272
		 3.033229828 0.22395825 4.81977272 3.033229589 0.22395825 -4.73569536 -4.097939968 0.22395825 -4.73569536
		 2.65081382 0.22395825 4.81977272 2.65081382 0.22395825 -4.73569536 2.65081382 0 -4.73569536
		 2.65081382 0 4.81977272 3.033229589 0.22395825 1.35070252 2.65081382 0.22395825 1.35070252
		 -4.097939968 0.22395825 1.35070252 -4.097939968 0 1.35070252 2.65081382 0 1.35070252
		 3.041473389 -0.0071774721 1.35070252 5.70698023 -0.0071774721 1.3507024 5.68321466 0.22395825 1.35070252
		 5.68321466 0 -4.73569536 5.68321466 0.22395825 -4.73569536 3.71552467 -2.68479586 1.35070252
		 3.71552467 -2.68479586 4.81977272 4.097940445 -2.68479586 1.35070252 4.097940922 -2.68479586 4.81977272
		 4.097940922 -2.51147866 4.8197732 4.097940445 -2.51147866 1.35070252 3.71552467 -2.51147866 1.35070252
		 3.71552467 -2.51147866 4.8197732 5.73074722 -2.51147866 4.8197732 5.73074579 -2.51147866 1.35070229
		 5.73074722 -2.68479586 4.81977272 5.73074579 -2.68479586 1.35070229;
	setAttr -s 65 ".ed[0:64]"  0 11 0 0 15 0 1 17 1 2 10 0 0 4 0 1 5 0 4 8 0
		 3 6 1 5 12 0 2 7 0 7 9 0 4 14 0 8 5 0 9 6 0 8 13 1 10 3 0 9 10 1 11 1 1 10 16 1 11 8 1
		 12 6 1 13 9 1 12 13 1 14 7 0 13 14 1 15 2 0 14 15 1 16 11 0 15 16 1 17 3 1 16 17 0
		 17 12 0 17 18 0 12 19 0 18 19 0 3 20 0 18 20 0 6 21 0 20 21 0 19 21 0 16 28 0 11 29 0
		 22 23 0 17 27 0 22 24 0 1 26 0 25 24 1 23 25 0 26 25 1 27 24 1 26 27 0 28 22 0 27 28 1
		 29 23 0 28 29 1 29 26 1 26 30 0 27 31 0 30 31 0 25 32 0 30 32 0 24 33 0 32 33 0 31 33 0
		 31 18 0;
	setAttr -s 33 -ch 132 ".fc[0:32]" -type "polyFaces" 
		f 4 6 14 24 -12
		mu 0 4 8 12 19 20
		f 4 1 28 27 -1
		mu 0 4 4 21 23 17
		f 4 0 19 -7 -5
		mu 0 4 0 16 12 8
		f 4 2 31 -9 -6
		mu 0 4 1 24 18 9
		f 4 -4 9 10 16
		mu 0 4 15 2 11 13
		f 4 -2 4 11 26
		mu 0 4 22 0 8 20
		f 4 12 8 22 -15
		mu 0 4 12 9 18 19
		f 4 -16 -17 13 -8
		mu 0 4 3 15 13 10
		f 4 -43 44 -47 -48
		mu 0 4 30 31 32 33
		f 4 -20 17 5 -13
		mu 0 4 12 16 1 9
		f 4 -23 20 -14 -22
		mu 0 4 19 18 10 13
		f 4 -25 21 -11 -24
		mu 0 4 20 19 13 11
		f 4 -26 -27 23 -10
		mu 0 4 2 22 20 11
		f 4 -29 25 3 18
		mu 0 4 23 21 7 14
		f 4 -31 -19 15 -30
		mu 0 4 25 23 14 6
		f 4 -35 36 38 -40
		mu 0 4 26 27 28 29
		f 4 -32 32 34 -34
		mu 0 4 18 24 27 26
		f 4 29 35 -37 -33
		mu 0 4 24 3 28 27
		f 4 7 37 -39 -36
		mu 0 4 3 10 29 28
		f 4 -21 33 39 -38
		mu 0 4 10 18 26 29
		f 4 -28 40 54 -42
		mu 0 4 17 23 36 37
		f 4 30 43 52 -41
		mu 0 4 23 25 35 36
		f 4 -3 45 50 -44
		mu 0 4 25 5 34 35
		f 4 -18 41 55 -46
		mu 0 4 5 17 37 34
		f 4 -59 60 62 -64
		mu 0 4 38 39 40 41
		f 4 -53 49 -45 -52
		mu 0 4 36 35 32 31
		f 4 -55 51 42 -54
		mu 0 4 37 36 31 30
		f 4 -56 53 47 -49
		mu 0 4 34 37 30 33
		f 4 -51 56 58 -58
		mu 0 4 35 34 39 38
		f 4 48 59 -61 -57
		mu 0 4 34 33 40 39
		f 4 46 61 -63 -60
		mu 0 4 33 32 41 40
		f 4 -50 57 63 -62
		mu 0 4 32 35 38 41
		f 4 57 64 -33 43
		mu 0 4 42 43 44 45;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "window_front" -p "crane_game";
	setAttr ".t" -type "double3" 0 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.437168977868605 1.7677271288944509 3.1245448055474419 ;
createNode mesh -n "window_frontShape" -p "window_front";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.6302352 0.22695127
		 0.6302352 0.137428 0.86902201 0.137428 0.86902207 0.22695127;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  1.19953156 3.28222513 1.45507038 1.19953156 -0.72912312 1.45507038
		 1.19953156 -0.72912359 -1.44573522 1.19953179 3.28222275 -1.44573522;
	setAttr -s 4 ".ed[0:3]"  1 0 0 1 2 0 2 3 0 0 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 -1 1 2 -4
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "window_back" -p "crane_game";
	setAttr ".t" -type "double3" 0 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.437168977868605 1.7677271288944509 3.1245448055474419 ;
createNode mesh -n "window_backShape" -p "window_back";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.13097802 0.137428
		 0.36976486 0.137428 0.36976486 0.22695127 0.13097802 0.22695127;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -1.19953156 -0.72912359 -1.44573522 -1.19953156 -0.72912359 1.45507038
		 -1.19953156 3.28222275 1.45507038 -1.19953179 3.28222275 -1.44573522;
	setAttr -s 4 ".ed[0:3]"  0 1 0 2 1 0 3 2 0 3 0 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 -2 -3 3
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "window_right" -p "crane_game";
	setAttr ".t" -type "double3" 0 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.437168977868605 1.7677271288944509 3.1245448055474419 ;
createNode mesh -n "window_rightShape" -p "window_right";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.37500003 0.52304876
		 0.625 0.52304876 0.625 0.61257195 0.375 0.61257195;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -1.18103349 3.28222275 -1.54642355 1.18103349 3.28222275 -1.54642355
		 1.18103349 -1.58597016 -1.54642355 -1.18103349 -1.58597016 -1.54642355;
	setAttr -s 4 ".ed[0:3]"  1 0 0 1 2 0 2 3 0 0 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 -1 1 2 -4
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "window_left" -p "crane_game";
	setAttr ".t" -type "double3" 0 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.437168977868605 1.7677271288944509 3.1245448055474419 ;
createNode mesh -n "window_leftShape" -p "window_left";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 4 ".uvst[0].uvsp[0:3]" -type "float2" 0.375 0.137428 0.625
		 0.137428 0.625 0.22695127 0.375 0.22695127;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 4 ".vt[0:3]"  -1.18103349 -1.58597016 1.54642355 1.18103349 -1.58597016 1.54642355
		 1.18103349 3.28222275 1.54642355 -1.18103349 3.28222275 1.54642355;
	setAttr -s 4 ".ed[0:3]"  0 1 0 1 2 0 3 2 0 0 3 0;
	setAttr -ch 4 ".fc[0]" -type "polyFaces" 
		f 4 0 1 -3 -4
		mu 0 4 0 1 2 3;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "outer_case" -p "crane_game";
	setAttr ".t" -type "double3" 0 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.437168977868605 1.7677271288944509 3.1245448055474419 ;
createNode mesh -n "outer_caseShape" -p "outer_case";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 104 ".uvst[0].uvsp[0:103]" -type "float2" 0.375 0.24628329
		 0.625 0.24628329 0.625 0.25 0.375 0.25 0.625 0.25523517 0.37500003 0.25523517 0.375
		 0.5 0.625 0.5 0.625 0.50371671 0.375 0.50371671 0.375 0.9947648 0.625 0.9947648 0.625
		 1 0.375 1 0.6302352 0.24628329 0.6302352 0.25 0.36976486 0.24628329 0.36976486 0.25
		 0.375 0.137428 0.375 0.22695127 0.36976486 0.22695127 0.36976486 0.137428 0.36976486
		 0.0036428806 0.375 0.0036428806 0.625 0.0036428806 0.625 0.137428 0.375 0.61257195
		 0.625 0.61257195 0.625 0.74635714 0.375 0.74635714 0.13097802 0.24628329 0.13097802
		 0.25 0.13097802 0.0036428806 0.13097802 0.137428 0.375 0.75597805 0.625 0.75597805
		 0.86902207 0.24628329 0.86902201 0.25 0.625 0.49402198 0.375 0.49402198 0.125 0.24628329
		 0.125 0.25 0.13097802 0.22695127 0.125 0.22695127 0.125 0.137428 0.125 0.0036428806
		 0.375 0.75 0.625 0.75 0.875 0.24628329 0.875 0.25 0.36976486 0 0.375 0 0.625 0 0.125
		 0 0.13097802 0 0.625 0.22695127 0.6302352 0.22695127 0.86902207 0.22695127 0.87500006
		 0.22695127 0.625 0.52304876 0.37500003 0.52304876 0.625 0.137428 0.375 0.137428 0.625
		 0.22695127 0.375 0.22695127 0.625 0.52304876 0.37500003 0.52304876 0.625 0.61257195
		 0.375 0.61257195 0.36976486 0.137428 0.13097802 0.137428 0.36976486 0.22695127 0.13097802
		 0.22695127 0.6302352 0.22695127 0.86902207 0.22695127 0.63023514 0 0.63023514 0.0036428806
		 0.86902201 0 0.86902201 0.0036428806 0.875 0 0.875 0.0036428806 0.6302352 0.137428
		 0.6302352 0.137428 0.86902201 0.137428 0.86902201 0.137428 0.6302352 0.04179696 0.6302352
		 0.079002023 0.875 0.137428 0.86902201 0.079002023 0.86902201 0.04179696 0.65407717
		 0.079002023 0.67791921 0.079002023 0.67651695 0.04179696 0.6533761 0.04179696 0.6533761
		 0.04179696 0.65407717 0.079002023 0.67651695 0.04179696 0.67791921 0.079002023 0.86902201
		 0.079002023 0.86902201 0.137428 0.6302352 0.137428 0.6302352 0.079002023 0.65407717
		 0.079002023 0.67791921 0.079002023;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 82 ".vt[0:81]"  -1.22175884 -4.08402586 1.57087505 1.22175884 -4.08402586 1.57087505
		 -1.22175884 4.084024906 1.57087505 1.22175884 4.084024906 1.57087505 -1.22175884 4.084024906 -1.57087505
		 1.22175884 4.084024906 -1.57087505 -1.22175884 -4.08402586 -1.57087505 1.22175884 -4.08402586 -1.57087505
		 -1.22175896 3.33097124 -1.57087505 -1.22175884 3.33097124 1.57087505 1.22175884 3.33097124 1.57087505
		 1.22175896 3.33097124 -1.57087505 -1.22175884 -1.63471889 -1.57087505 -1.22175884 -1.63471889 1.57087505
		 1.22175884 -1.63471889 1.57087505 1.22175884 -1.63471889 -1.57087505 -1.22175884 4.084024906 1.5050844
		 -1.22175884 3.33097124 1.5050844 -1.22175884 -0.77787209 1.5050844 -1.22175884 -4.08402586 1.5050844
		 1.22175884 -4.08402586 1.5050844 1.22175884 -0.77787209 1.5050844 1.22175884 3.33097363 1.5050844
		 1.22175884 4.084024906 1.5050844 -1.22175884 4.084024906 -1.49574924 -1.22175896 3.33097124 -1.49574924
		 -1.22175884 -0.77787209 -1.49574924 -1.22175884 -4.08402586 -1.49574924 1.22175884 -4.08402586 -1.49574924
		 1.22175884 -0.77787209 -1.49574924 1.22175896 3.33097124 -1.49574924 1.22175884 4.084024906 -1.49574924
		 -1.22175884 -3.96500516 1.5050844 -1.22175884 -3.96500516 1.57087505 1.22175884 -3.96500516 1.57087505
		 1.22175884 -3.96500516 1.5050844 1.22175884 -3.96500516 -1.49574924 1.22175884 -3.96500516 -1.57087505
		 -1.22175884 -3.96500516 -1.57087505 -1.22175884 -3.96500516 -1.49574924 -1.22175884 3.96259069 1.5050844
		 -1.22175884 3.96259069 1.57087505 1.22175884 3.96259069 1.57087505 1.22175884 3.96259069 1.5050844
		 1.22175884 3.96259069 -1.49574924 1.22175884 3.96259069 -1.57087505 -1.22175884 3.96259069 -1.57087505
		 -1.22175884 3.96259069 -1.49574924 -1.18103349 -1.5859704 1.54642355 1.18103349 -1.5859704 1.54642355
		 1.18103349 3.28222275 1.54642355 -1.18103349 3.28222275 1.54642355 1.18103349 3.28222275 -1.54642355
		 -1.18103349 3.28222275 -1.54642355 1.18103349 -1.5859704 -1.54642355 -1.18103349 -1.5859704 -1.54642355
		 -1.19953156 -0.72912359 -1.44573522 -1.19953156 -0.72912359 1.45507038 -1.19953156 3.28222275 1.45507038
		 -1.19953179 3.28222275 -1.44573522 1.19953156 -0.72912335 1.45507038 1.19953156 3.28222513 1.45507038
		 1.19953156 -0.72912359 -1.44573522 1.19953179 3.28222275 -1.44573522 1.22175884 -1.9592607 -1.49574924
		 1.22175884 -1.95926046 1.5050844 1.22175884 -3.056069374 -1.49574924 1.22175884 -3.056069374 1.5050844
		 1.22175884 -3.056069374 0.67095643 1.22175884 -1.95926046 0.6533305 1.22175884 -3.056069374 1.24103832
		 1.22175884 -1.95926046 1.23222554 1.18681097 -3.056069374 1.24103832 1.18681097 -1.95926046 1.23222554
		 1.18681097 -3.056069374 0.67095643 1.18681097 -1.95926046 0.6533305 1.22175884 -0.77787209 -1.49574924
		 1.22175884 -1.9592607 -1.49574924 1.22175884 -0.77787209 1.5050844 1.22175884 -1.95926046 1.5050844
		 1.22175884 -1.95926046 1.23222554 1.22175884 -1.95926046 0.6533305;
	setAttr -s 152 ".ed[0:151]"  0 1 0 2 3 0 4 5 0 6 7 0 0 33 0 1 34 0 2 16 0
		 3 23 0 4 46 0 5 45 0 6 27 0 7 28 0 8 12 0 9 41 0 8 25 1 10 42 0 9 10 0 11 15 0 10 22 1
		 11 8 0 12 38 0 13 9 0 14 10 0 13 14 0 15 37 0 15 12 0 16 24 0 17 9 1 16 40 1 17 18 1
		 19 0 0 18 32 1 20 1 0 19 20 1 22 30 0 23 31 0 22 43 1 23 16 1 24 4 0 25 17 0 24 47 1
		 26 18 0 25 26 1 27 19 0 26 39 1 28 20 0 27 28 1 30 11 1 31 5 0 30 44 1 31 24 1 32 19 1
		 33 13 0 32 33 1 34 14 0 33 34 1 37 7 0 38 6 0 37 38 1 39 27 1 38 39 1 39 32 1 40 17 1
		 41 2 0 40 41 1 42 3 0 41 42 1 43 23 1 42 43 1 44 31 1 43 44 1 45 11 0 44 45 1 46 8 0
		 45 46 1 47 25 1 46 47 1 47 40 1 13 48 0 14 49 0 48 49 0 10 50 0 49 50 0 9 51 0 51 50 0
		 48 51 0 11 52 0 8 53 0 52 53 0 15 54 0 52 54 0 12 55 0 54 55 0 53 55 0 26 56 0 18 57 0
		 56 57 0 17 58 0 58 57 0 25 59 0 59 58 0 59 56 0 22 61 0 60 61 0 60 62 0 30 63 0 62 63 0
		 61 63 0 34 35 1 35 36 1 36 37 1 20 35 1 28 36 1 21 60 0 21 29 0 29 62 0 29 30 1 21 22 1
		 35 67 1 66 68 1 68 70 0 36 66 1 29 64 0 64 66 1 21 65 0 65 71 0 64 69 0 68 69 0 69 71 0
		 65 67 1 67 70 1 70 71 0 70 72 0 71 73 0 72 73 0 68 74 0 74 72 0 69 75 0 74 75 0 75 73 0
		 29 76 0 64 77 0 76 77 0 21 78 0 78 76 0 65 79 0 78 79 0 71 80 0 79 80 0 69 81 0 81 80 0
		 77 81 0;
	setAttr -s 67 -ch 284 ".fc[0:66]" -type "polyFaces" 
		f 4 66 65 -2 -64
		mu 0 4 0 1 2 3
		f 4 1 7 37 -7
		mu 0 4 3 2 4 5
		f 4 2 9 74 -9
		mu 0 4 6 7 8 9
		f 4 33 32 -1 -31
		mu 0 4 10 11 12 13
		f 4 68 67 -8 -66
		mu 0 4 1 14 15 2
		f 4 64 63 6 28
		mu 0 4 16 0 3 17
		f 6 21 -28 29 31 53 52
		mu 0 6 18 19 20 21 22 23
		f 4 55 54 -24 -53
		mu 0 4 23 24 25 18
		f 4 -26 24 58 -21
		mu 0 4 26 27 28 29
		f 4 77 -29 26 40
		mu 0 4 30 16 17 31
		f 4 61 -32 -42 44
		mu 0 4 32 22 21 33
		f 4 46 45 -34 -44
		mu 0 4 34 35 11 10
		f 4 -68 70 69 -36
		mu 0 4 15 14 36 37
		f 4 -38 35 50 -27
		mu 0 4 5 4 38 39
		f 4 76 -41 38 8
		mu 0 4 40 30 31 41
		f 6 -43 -15 12 20 60 -45
		mu 0 6 33 42 43 44 45 32
		f 4 3 11 -47 -11
		mu 0 4 46 47 35 34
		f 4 -70 72 -10 -49
		mu 0 4 37 36 48 49
		f 4 -51 48 -3 -39
		mu 0 4 39 38 7 6
		f 4 30 4 -54 51
		mu 0 4 50 51 23 22
		f 4 0 5 -56 -5
		mu 0 4 51 52 24 23
		f 4 -59 56 -4 -58
		mu 0 4 29 28 47 46
		f 4 10 -60 -61 57
		mu 0 4 53 54 32 45
		f 4 43 -52 -62 59
		mu 0 4 54 50 22 32
		f 4 27 13 -65 62
		mu 0 4 20 19 0 16
		f 4 16 15 -67 -14
		mu 0 4 19 55 1 0
		f 4 18 36 -69 -16
		mu 0 4 55 56 14 1
		f 4 -71 -37 34 49
		mu 0 4 36 14 56 57
		f 4 -73 -50 47 -72
		mu 0 4 48 36 57 58
		f 4 -75 71 19 -74
		mu 0 4 9 8 59 60
		f 4 14 -76 -77 73
		mu 0 4 43 42 30 40
		f 4 39 -63 -78 75
		mu 0 4 42 20 16 30
		f 4 23 79 -81 -79
		mu 0 4 18 25 61 62
		f 4 22 81 -83 -80
		mu 0 4 25 55 63 61
		f 4 -17 83 84 -82
		mu 0 4 55 19 64 63
		f 4 -22 78 85 -84
		mu 0 4 19 18 62 64
		f 4 -20 86 88 -88
		mu 0 4 60 59 65 66
		f 4 17 89 -91 -87
		mu 0 4 59 27 67 65
		f 4 25 91 -93 -90
		mu 0 4 27 26 68 67
		f 4 -13 87 93 -92
		mu 0 4 26 60 66 68
		f 4 41 95 -97 -95
		mu 0 4 33 21 69 70
		f 4 -30 97 98 -96
		mu 0 4 21 20 71 69
		f 4 -40 99 100 -98
		mu 0 4 20 42 72 71
		f 4 42 94 -102 -100
		mu 0 4 42 33 70 72
		f 4 -35 102 107 -106
		mu 0 4 57 56 73 74
		f 4 -6 -33 111 -109
		mu 0 4 24 52 75 76
		f 4 112 -110 -112 -46
		mu 0 4 77 78 76 75
		f 4 -57 -111 -113 -12
		mu 0 4 79 80 78 77
		f 4 -103 -118 113 103
		mu 0 4 73 56 81 82
		f 4 -114 114 115 -105
		mu 0 4 82 81 83 84
		f 4 -116 116 105 -107
		mu 0 4 84 83 57 74
		f 8 117 -19 -23 -55 108 118 -130 -125
		mu 0 8 81 56 55 25 24 76 85 86
		f 8 -18 -48 -117 122 123 -122 110 -25
		mu 0 8 87 58 57 83 88 89 78 80
		f 6 -143 -145 146 148 -151 -152
		mu 0 6 98 99 100 101 102 103
		f 4 -128 -120 -124 126
		mu 0 4 91 92 89 88
		f 6 120 -131 -119 109 121 119
		mu 0 6 92 93 85 76 78 89
		f 4 131 -126 129 130
		mu 0 4 93 90 86 85
		f 4 -132 132 134 -134
		mu 0 4 90 93 94 95
		f 4 -121 135 136 -133
		mu 0 4 93 92 96 94
		f 4 127 137 -139 -136
		mu 0 4 92 91 97 96
		f 4 128 133 -140 -138
		mu 0 4 91 90 95 97
		f 4 -123 140 142 -142
		mu 0 4 88 83 99 98
		f 4 -115 143 144 -141
		mu 0 4 83 81 100 99
		f 4 124 145 -147 -144
		mu 0 4 81 86 101 100
		f 4 125 147 -149 -146
		mu 0 4 86 90 102 101
		f 4 -129 149 150 -148
		mu 0 4 90 91 103 102
		f 4 -127 141 151 -150
		mu 0 4 91 88 98 103;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "prize_door" -p "crane_game";
	setAttr ".t" -type "double3" -0.0093992631093042789 7.2088559365105151 0 ;
	setAttr ".s" -type "double3" 3.3360648180155317 1.7157295205815808 3.0326364706584035 ;
	setAttr ".rp" -type "double3" 4.1599182312514298 -4.4328670965861185 2.9595201396327648 ;
	setAttr ".sp" -type "double3" 1.2102745771408081 -2.5076647996902466 0.94718441367149353 ;
	setAttr ".spt" -type "double3" 2.9496436541106212 -1.9252022968958717 2.0123357259612713 ;
createNode mesh -n "prize_doorShape" -p "prize_door";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 12 ".uvst[0].uvsp[0:11]" -type "float2" 0.65407717 0.079002023
		 0.6533761 0.04179696 0.67651695 0.04179696 0.67791921 0.079002023 0.65407717 0.079002023
		 0.6533761 0.04179696 0.67651695 0.04179696 0.67791921 0.079002023 0.65407717 0.079002023
		 0.6533761 0.04179696 0.67651695 0.04179696 0.67791921 0.079002023;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  1.18681097 -1.95926023 1.23222542 1.18681097 -3.056069374 1.24103832
		 1.18681097 -3.056069374 0.67095643 1.18681097 -1.95926023 0.6533305 1.23373818 -3.056069374 1.24103832
		 1.23373818 -1.95926023 1.23222542 1.23373818 -3.056069374 0.67095643 1.23373818 -1.95926023 0.6533305;
	setAttr -s 12 ".ed[0:11]"  1 0 0 2 1 0 2 3 0 3 0 0 1 4 0 0 5 0 4 5 0
		 2 6 0 6 4 0 3 7 0 6 7 0 7 5 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 -7 -9 10 11
		mu 0 4 8 9 10 11
		f 4 -4 -3 1 0
		mu 0 4 4 7 6 5
		f 4 -1 4 6 -6
		mu 0 4 0 1 9 8
		f 4 -2 7 8 -5
		mu 0 4 1 2 10 9
		f 4 2 9 -11 -8
		mu 0 4 2 3 11 10
		f 4 3 5 -12 -10
		mu 0 4 3 0 8 11;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "control_arm_assembly" -p "crane_game";
	setAttr ".rp" -type "double3" 0.5136033937086304 13.637004239878353 0.096307003607841501 ;
	setAttr ".sp" -type "double3" 0.5136033937086304 13.637004239878353 0.096307003607841501 ;
createNode transform -n "control_arm_mount" -p "control_arm_assembly";
	setAttr ".t" -type "double3" 0.5136033937086304 13.515550001354514 2.881871400684358 ;
createNode mesh -n "control_arm_mountShape" -p "control_arm_mount";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.84520322 -0.14755242 0.89127421 0.84520322 -0.14755242 0.89127421
		 -0.84520322 0.14755242 0.89127421 0.84520322 0.14755242 0.89127421 -0.84520322 0.14755242 -0.89127421
		 0.84520322 0.14755242 -0.89127421 -0.84520322 -0.14755242 -0.89127421 0.84520322 -0.14755242 -0.89127421;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crane_claw" -p "control_arm_mount";
	setAttr ".t" -type "double3" 0.29393703009041305 -2.587912265473582 -0.041079367675312284 ;
	setAttr ".s" -type "double3" 0.28036440368231663 0.28036440368231663 0.28036440368231663 ;
	setAttr ".rp" -type "double3" -0.25806890245477854 1.446634090959356 0.015534330878342986 ;
	setAttr ".sp" -type "double3" -0.92047670483589172 5.1598351001739502 0.05540764331817627 ;
	setAttr ".spt" -type "double3" 0.66240780238111319 -3.7132010092145942 -0.039873312439833282 ;
createNode transform -n "claw_1" -p "crane_claw";
	setAttr ".rp" -type "double3" -0.92104681425690138 5.4217192927525373 -0.55716177141876266 ;
	setAttr ".sp" -type "double3" -0.92104681425690138 5.4217192927525373 -0.55716177141876266 ;
createNode mesh -n "claw_Shape1" -p "claw_1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 96 ".uvst[0].uvsp[0:95]" -type "float2" 0.40000001 0.3125
		 0.42500001 0.3125 0.42500001 0.42888451 0.40000001 0.42888451 0.40000001 0.3125 0.42500001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.40000001
		 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.3125 0.42500001 0.3125;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".pt[0:95]" -type "float3"  0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 
		0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2 0 0.14198092 0.2;
	setAttr -s 96 ".vt[0:95]"  -0.68187004 4.92126322 -1.20473504 -0.72854537 4.97333002 -1.58686924
		 -0.77522081 5.13678455 -1.9690032 -1.15908384 4.92126322 -1.20473492 -1.1124084 4.97333002 -1.586869
		 -1.065733194 5.13678455 -1.9690032 -1.15908384 5.4535985 -1.20473492 -1.1124084 5.4015317 -1.586869
		 -1.065733194 5.46085262 -1.9690032 -0.68187004 5.4535985 -1.20473504 -0.72854537 5.4015317 -1.58686924
		 -0.77522081 5.46085262 -1.9690032 -0.78631347 5.1188674 -2.29622531 -0.78600055 5.089936733 -2.49717855
		 -0.78600055 5.007376194 -2.59324694 -0.78600055 4.87451887 -2.6260674 -0.78600055 4.6663518 -2.65032482
		 -0.78600055 4.43654871 -2.65496016 -0.78600055 4.22586107 -2.64865661 -0.78600055 4.024241924 -2.63829374
		 -0.78600055 3.81147575 -2.63030767 -0.78600055 3.60030699 -2.61093307 -0.78600055 3.42376661 -2.54635477
		 -0.78600055 3.28423929 -2.41247368 -0.78600055 3.14552736 -2.20244741 -0.78600055 3.0071976185 -1.95517468
		 -0.78600055 2.87478685 -1.72840393 -0.78600055 2.74897218 -1.53467774 -0.78600055 2.49944901 -1.35571861
		 -0.78600055 2.31310081 -1.16756785 -0.78600055 2.23653769 -0.96332455 -0.78600055 2.11380792 -0.88733327
		 -1.076661348 5.119133 -2.28645587 -1.076512098 5.089263439 -2.49708414 -1.076512098 5.0068092346 -2.59287119
		 -1.076512098 4.87423897 -2.62544751 -1.076512098 4.66625357 -2.64965153 -1.076512098 4.4365325 -2.65427971
		 -1.076512098 4.22588158 -2.64797616 -1.076512098 4.024277687 -2.63761377 -1.076512098 3.81150198 -2.62962723
		 -1.076512098 3.60036922 -2.610255 -1.076512098 3.42398596 -2.54571009 -1.076512098 3.28468132 -2.41195583
		 -1.076512098 3.14608765 -2.2020607 -1.076512098 3.0077910423 -1.95484161 -1.076512098 2.87537479 -1.72806132
		 -1.076512098 2.74954295 -1.53430712 -1.076512098 2.5000155 -1.35534167 -1.076512098 2.31367803 -1.16720772
		 -1.076512098 2.2371242 -0.96297967 -1.076512098 2.11439848 -0.88699538 -1.076957703 5.44272232 -2.30405831
		 -1.077270627 5.41019964 -2.54201937 -1.077270627 5.27701139 -2.77178431 -1.077270627 5.0077228546 -2.92074609
		 -1.077270627 4.7133317 -2.97028065 -1.077270627 4.44472075 -2.97824311 -1.077270627 4.21655226 -2.97190881
		 -1.077270627 4.007707119 -2.96125674 -1.077270627 3.79936934 -2.95346689 -1.077270627 3.57110548 -2.9329977
		 -1.077270627 3.31996632 -2.85262895 -1.077270627 3.074559927 -2.6586709 -1.077270627 2.8795948 -2.38645434
		 -1.077270627 2.72536039 -2.11375141 -1.077270627 2.59553456 -1.89148951 -1.077270627 2.47790623 -1.71103525
		 -1.077270627 2.36060238 -1.53511143 -1.077270627 2.23791647 -1.33899355 -1.077270627 2.11321926 -1.12752116
		 -1.077270627 2.02619195 -0.90383929 -0.78660971 5.44245672 -2.31382799 -0.78675902 5.41087341 -2.54211354
		 -0.78675902 5.27757883 -2.77216005 -0.78675902 5.0080032349 -2.92136598 -0.78675902 4.71343088 -2.97095346
		 -0.78675902 4.44473791 -2.97892308 -0.78675902 4.21653271 -2.97258878 -0.78675902 4.0076723099 -2.96193624
		 -0.78675902 3.79934359 -2.95414686 -0.78675902 3.57104373 -2.93367529 -0.78675902 3.31974745 -2.85327315
		 -0.78675902 3.074118614 -2.65918827 -0.78675902 2.87903547 -2.38684082 -0.78675902 2.72476745 -2.11408424
		 -0.78675902 2.59494686 -1.89183187 -0.78675902 2.47733545 -1.71140552 -0.78675902 2.3600359 -1.53548813
		 -0.78675902 2.23733902 -1.33935344 -0.78675902 2.11263275 -1.12786579 -0.78675902 2.025601149 -0.90417689
		 -1.20575905 5.61705303 -0.82260078 -0.6351946 5.61705303 -0.8226009 -0.6351946 4.98058414 -0.8226009
		 -1.20575905 4.98058414 -0.82260078;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 3 4 0 4 5 0 0 3 1 1 4 1 2 5 1 92 6 0 6 7 0
		 7 8 0 3 6 1 4 7 1 5 8 1 93 9 0 9 10 0 10 11 0 6 9 1 7 10 1 8 11 1 0 9 1 1 10 1 2 11 1
		 2 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0 18 19 0 19 20 0 20 21 0 21 22 0
		 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0 29 30 0 30 31 0 5 32 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 40 0 40 41 0 41 42 0 42 43 0 43 44 0
		 44 45 0 45 46 0 46 47 0 47 48 0 48 49 0 49 50 0 50 51 0 12 32 1 13 33 0 14 34 0 15 35 1
		 16 36 1 17 37 1 18 38 1 19 39 1 20 40 1 21 41 1 22 42 1 23 43 1 24 44 1 25 45 1 26 46 1
		 27 47 1 28 48 1 29 49 1 30 50 1 31 51 0 8 52 0 52 53 0 53 54 0 54 55 0 55 56 0 56 57 0
		 57 58 0 58 59 0 59 60 0 60 61 0 61 62 0 62 63 0 63 64 0 64 65 0 65 66 0 66 67 0 67 68 0
		 68 69 0 69 70 0 70 71 0 32 52 1 33 53 1 34 54 1 35 55 1 36 56 1 37 57 1 38 58 1 39 59 1
		 40 60 1 41 61 1 42 62 1 43 63 1 44 64 1 45 65 1 46 66 1 47 67 1 48 68 1 49 69 1 50 70 1
		 51 71 0 11 72 0 72 73 0 73 74 0 74 75 0 75 76 0 76 77 0 77 78 0 78 79 0 79 80 0 80 81 0
		 81 82 0 82 83 0 83 84 0 84 85 0 85 86 0 86 87 0 87 88 0 88 89 0 89 90 0 90 91 0 52 72 1
		 53 73 1 54 74 0 55 75 1 56 76 1 57 77 1 58 78 1 59 79 1 60 80 1 61 81 1 62 82 1 63 83 1
		 64 84 1 65 85 1 66 86 1 67 87 1 68 88 1 69 89 1 70 90 1 71 91 0 12 72 1 13 73 1 14 74 1
		 15 75 1;
	setAttr ".ed[166:187]" 16 76 1 17 77 1 18 78 1 19 79 1 20 80 1 21 81 1 22 82 1
		 23 83 1 24 84 1 25 85 1 26 86 1 27 87 1 28 88 1 29 89 1 30 90 1 31 91 0 95 3 0 94 0 0
		 92 93 0 94 95 0 95 92 0 94 93 0;
	setAttr -s 93 -ch 372 ".fc[0:92]" -type "polyFaces" 
		f 4 81 121 161 -182
		mu 0 4 0 1 2 3
		f 4 4 2 -6 -1
		mu 0 4 4 5 6 7
		f 4 5 3 -7 -2
		mu 0 4 7 6 8 9
		f 4 10 8 -12 -3
		mu 0 4 5 10 11 6
		f 4 11 9 -13 -4
		mu 0 4 6 11 12 8
		f 4 184 13 -17 -8
		mu 0 4 13 14 15 10
		f 4 16 14 -18 -9
		mu 0 4 10 15 16 11
		f 4 17 15 -19 -10
		mu 0 4 11 16 17 12
		f 4 -20 0 20 -15
		mu 0 4 15 4 7 16
		f 4 -21 1 21 -16
		mu 0 4 16 7 9 17
		f 4 6 42 -63 -23
		mu 0 4 9 8 18 19
		f 4 62 43 -64 -24
		mu 0 4 19 18 20 21
		f 4 63 44 -65 -25
		mu 0 4 21 20 22 23
		f 4 64 45 -66 -26
		mu 0 4 23 22 24 25
		f 4 65 46 -67 -27
		mu 0 4 25 24 26 27
		f 4 66 47 -68 -28
		mu 0 4 27 26 28 29
		f 4 67 48 -69 -29
		mu 0 4 29 28 30 31
		f 4 68 49 -70 -30
		mu 0 4 31 30 32 33
		f 4 69 50 -71 -31
		mu 0 4 33 32 34 35
		f 4 70 51 -72 -32
		mu 0 4 35 34 36 37
		f 4 71 52 -73 -33
		mu 0 4 37 36 38 39
		f 4 72 53 -74 -34
		mu 0 4 39 38 40 41
		f 4 73 54 -75 -35
		mu 0 4 41 40 42 43
		f 4 74 55 -76 -36
		mu 0 4 43 42 44 45
		f 4 75 56 -77 -37
		mu 0 4 45 44 46 47
		f 4 76 57 -78 -38
		mu 0 4 47 46 48 49
		f 4 77 58 -79 -39
		mu 0 4 49 48 50 51
		f 4 78 59 -80 -40
		mu 0 4 51 50 52 53
		f 4 79 60 -81 -41
		mu 0 4 53 52 54 55
		f 4 80 61 -82 -42
		mu 0 4 55 54 1 0
		f 4 12 82 -103 -43
		mu 0 4 8 12 56 18
		f 4 102 83 -104 -44
		mu 0 4 18 56 57 20
		f 4 103 84 -105 -45
		mu 0 4 20 57 58 22
		f 4 104 85 -106 -46
		mu 0 4 22 58 59 24
		f 4 105 86 -107 -47
		mu 0 4 24 59 60 26
		f 4 106 87 -108 -48
		mu 0 4 26 60 61 28
		f 4 107 88 -109 -49
		mu 0 4 28 61 62 30
		f 4 108 89 -110 -50
		mu 0 4 30 62 63 32
		f 4 109 90 -111 -51
		mu 0 4 32 63 64 34
		f 4 110 91 -112 -52
		mu 0 4 34 64 65 36
		f 4 111 92 -113 -53
		mu 0 4 36 65 66 38
		f 4 112 93 -114 -54
		mu 0 4 38 66 67 40
		f 4 113 94 -115 -55
		mu 0 4 40 67 68 42
		f 4 114 95 -116 -56
		mu 0 4 42 68 69 44
		f 4 115 96 -117 -57
		mu 0 4 44 69 70 46
		f 4 116 97 -118 -58
		mu 0 4 46 70 71 48
		f 4 117 98 -119 -59
		mu 0 4 48 71 72 50
		f 4 118 99 -120 -60
		mu 0 4 50 72 73 52
		f 4 119 100 -121 -61
		mu 0 4 52 73 74 54
		f 4 120 101 -122 -62
		mu 0 4 54 74 2 1
		f 4 18 122 -143 -83
		mu 0 4 12 17 75 56
		f 4 142 123 -144 -84
		mu 0 4 56 75 76 57
		f 4 143 124 -145 -85
		mu 0 4 57 76 77 58
		f 4 144 125 -146 -86
		mu 0 4 58 77 78 59
		f 4 145 126 -147 -87
		mu 0 4 59 78 79 60
		f 4 146 127 -148 -88
		mu 0 4 60 79 80 61
		f 4 147 128 -149 -89
		mu 0 4 61 80 81 62
		f 4 148 129 -150 -90
		mu 0 4 62 81 82 63
		f 4 149 130 -151 -91
		mu 0 4 63 82 83 64
		f 4 150 131 -152 -92
		mu 0 4 64 83 84 65
		f 4 151 132 -153 -93
		mu 0 4 65 84 85 66
		f 4 152 133 -154 -94
		mu 0 4 66 85 86 67
		f 4 153 134 -155 -95
		mu 0 4 67 86 87 68
		f 4 154 135 -156 -96
		mu 0 4 68 87 88 69
		f 4 155 136 -157 -97
		mu 0 4 69 88 89 70
		f 4 156 137 -158 -98
		mu 0 4 70 89 90 71
		f 4 157 138 -159 -99
		mu 0 4 71 90 91 72
		f 4 158 139 -160 -100
		mu 0 4 72 91 92 73
		f 4 159 140 -161 -101
		mu 0 4 73 92 93 74
		f 4 160 141 -162 -102
		mu 0 4 74 93 3 2
		f 4 -22 22 162 -123
		mu 0 4 17 9 19 75
		f 4 -163 23 163 -124
		mu 0 4 75 19 21 76
		f 4 -164 24 164 -125
		mu 0 4 76 21 23 77
		f 4 -165 25 165 -126
		mu 0 4 77 23 25 78
		f 4 -166 26 166 -127
		mu 0 4 78 25 27 79
		f 4 -167 27 167 -128
		mu 0 4 79 27 29 80
		f 4 -168 28 168 -129
		mu 0 4 80 29 31 81
		f 4 -169 29 169 -130
		mu 0 4 81 31 33 82
		f 4 -170 30 170 -131
		mu 0 4 82 33 35 83
		f 4 -171 31 171 -132
		mu 0 4 83 35 37 84
		f 4 -172 32 172 -133
		mu 0 4 84 37 39 85
		f 4 -173 33 173 -134
		mu 0 4 85 39 41 86
		f 4 -174 34 174 -135
		mu 0 4 86 41 43 87
		f 4 -175 35 175 -136
		mu 0 4 87 43 45 88
		f 4 -176 36 176 -137
		mu 0 4 88 45 47 89
		f 4 -177 37 177 -138
		mu 0 4 89 47 49 90
		f 4 -178 38 178 -139
		mu 0 4 90 49 51 91
		f 4 -179 39 179 -140
		mu 0 4 91 51 53 92
		f 4 -180 40 180 -141
		mu 0 4 92 53 55 93
		f 4 -181 41 181 -142
		mu 0 4 93 55 0 3
		f 4 -184 185 182 -5
		mu 0 4 4 94 95 5
		f 4 186 7 -11 -183
		mu 0 4 95 13 10 5
		f 4 -14 -188 183 19
		mu 0 4 15 14 94 4;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "claw_base" -p "crane_claw";
	setAttr ".rp" -type "double3" -0.92047670483589172 6.5663361549377441 0.05540764331817627 ;
	setAttr ".sp" -type "double3" -0.92047670483589172 6.5663361549377441 0.05540764331817627 ;
createNode mesh -n "claw_baseShape" -p "claw_base";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 160 ".uvst[0].uvsp[0:159]" -type "float2" 0.45000002 0.50590676
		 0.47500002 0.50590676 0.47500002 0.63509905 0.45000005 0.63509905 0.42500001 0.50590676
		 0.42500001 0.63509905 0.40000001 0.50590676 0.40000001 0.63509905 0.375 0.50590676
		 0.375 0.63509905 0.5999999 0.50590676 0.62499988 0.50590676 0.62499988 0.63509905
		 0.5999999 0.63509905 0.45000002 0.42888451 0.47500002 0.42888451 0.47500002 0.50590676
		 0.45000002 0.50590676 0.42500001 0.42888451 0.42500001 0.50590676 0.40000001 0.42888451
		 0.40000001 0.50590676 0.375 0.42888451 0.375 0.50590676 0.5999999 0.42888451 0.62499988
		 0.42888451 0.62499988 0.50590676 0.5999999 0.50590676 0.40000001 0.42888451 0.42500001
		 0.42888451 0.375 0.42888451 0.62640893 0.93559146 0.54828393 0.9923526 0.54828393
		 0.9923526 0.62640893 0.93559146 0.4517161 0.9923526 0.4517161 0.9923526 0.37359107
		 0.93559146 0.37359107 0.93559146 0.34375 0.84375 0.34375 0.84375 0.65625 0.84375
		 0.65625 0.84375 0.375 0.65380532 0.40000004 0.65380532 0.40000001 0.68843985 0.375
		 0.68843985 0.5999999 0.65380532 0.62499988 0.65380532 0.62499988 0.68843985 0.5999999
		 0.68843985 0.45000005 0.65380532 0.47500002 0.65380532 0.47500002 0.68843985 0.45000002
		 0.68843985 0.42500001 0.65380532 0.42500001 0.68843985 0.65625 0.84375 0.62640893
		 0.93559146 0.54828393 0.9923526 0.4517161 0.9923526 0.37359107 0.93559146 0.34375
		 0.84375 0.375 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.45000002 0.3125 0.45000002
		 0.42888451 0.47500002 0.3125 0.47500002 0.42888451 0.5999999 0.42888451 0.5999999
		 0.3125 0.62499988 0.3125 0.62499988 0.42888451 0.54828387 0.0076473951 0.62640893
		 0.064408526 0.65625 0.15625 0.34375 0.15625001 0.37359107 0.064408556 0.45171607
		 0.00764741 0.45000002 0.50590676 0.45000005 0.63509905 0.47500002 0.63509905 0.47500002
		 0.50590676 0.42500001 0.50590676 0.42500001 0.63509905 0.40000001 0.50590676 0.40000001
		 0.63509905 0.375 0.50590676 0.375 0.63509905 0.5999999 0.50590676 0.5999999 0.63509905
		 0.62499988 0.63509905 0.62499988 0.50590676 0.45000002 0.42888451 0.45000002 0.50590676
		 0.47500002 0.50590676 0.47500002 0.42888451 0.42500001 0.42888451 0.42500001 0.50590676
		 0.40000001 0.42888451 0.40000001 0.50590676 0.375 0.42888451 0.375 0.50590676 0.5999999
		 0.42888451 0.5999999 0.50590676 0.62499988 0.50590676 0.62499988 0.42888451 0.40000001
		 0.42888451 0.42500001 0.42888451 0.375 0.42888451 0.62640893 0.93559146 0.62640893
		 0.93559146 0.54828393 0.9923526 0.54828393 0.9923526 0.4517161 0.9923526 0.4517161
		 0.9923526 0.37359107 0.93559146 0.37359107 0.93559146 0.34375 0.84375 0.34375 0.84375
		 0.65625 0.84375 0.65625 0.84375 0.375 0.65380532 0.375 0.68843985 0.40000001 0.68843985
		 0.40000004 0.65380532 0.5999999 0.65380532 0.5999999 0.68843985 0.62499988 0.68843985
		 0.62499988 0.65380532 0.45000005 0.65380532 0.45000002 0.68843985 0.47500002 0.68843985
		 0.47500002 0.65380532 0.42500001 0.65380532 0.42500001 0.68843985 0.65625 0.84375
		 0.34375 0.84375 0.37359107 0.93559146 0.4517161 0.9923526 0.54828393 0.9923526 0.62640893
		 0.93559146 0.40000001 0.3125 0.375 0.3125 0.45000002 0.42888451 0.45000002 0.3125
		 0.42500001 0.3125 0.47500002 0.42888451 0.47500002 0.3125 0.5999999 0.42888451 0.62499988
		 0.42888451 0.62499988 0.3125 0.5999999 0.3125 0.54828387 0.0076473951 0.45171607
		 0.00764741 0.37359107 0.064408556 0.34375 0.15625001 0.65625 0.15625 0.62640893 0.064408526;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 45 ".pt";
	setAttr ".pt[1]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[2]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[12]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[13]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[14]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[15]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[16]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[17]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[18]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[19]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[20]" -type "float3" 0 0.26712853 5.9604645e-08 ;
	setAttr ".pt[21]" -type "float3" 0 0.26712853 5.9604645e-08 ;
	setAttr ".pt[22]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[23]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[24]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[25]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[26]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[27]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[28]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[29]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[30]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[31]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[32]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[33]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[34]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[35]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[60]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[62]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[64]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[66]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[68]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[69]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[70]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[71]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[72]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[73]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[74]" -type "float3" 0 0.26712859 0 ;
	setAttr ".pt[75]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[76]" -type "float3" 0 0.26712853 5.9604645e-08 ;
	setAttr ".pt[77]" -type "float3" 0 0.26712853 5.9604645e-08 ;
	setAttr ".pt[78]" -type "float3" 0 0.26712853 0 ;
	setAttr ".pt[96]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[97]" -type "float3" 0 0 5.9604645e-08 ;
	setAttr ".pt[99]" -type "float3" 0 0.26712853 0 ;
	setAttr -s 100 ".vt[0:99]"  -0.17359823 4.98058414 -0.48723158 -0.6351946 4.98058414 -0.8226009
		 -1.20575905 4.98058414 -0.82260078 -1.6673553 4.98058414 -0.4872314 -1.84366941 4.98058414 0.055407614
		 0.002715826 4.98058414 0.055407614 -0.25890887 7.036477089 -0.42524979 -0.66778034 7.036477089 -0.72231221
		 -1.17317331 7.036477089 -0.72231215 -1.58204472 7.036477089 -0.42524964 -1.73821974 7.036477089 0.055407614
		 -0.10273389 7.036477089 0.055407614 -1.84366941 6.038262844 0.055407614 -1.6673553 6.038262844 -0.4872314
		 -1.20575905 6.038262844 -0.82260078 -0.6351946 6.038262844 -0.8226009 -0.17359823 6.038262844 -0.48723158
		 0.002715826 6.038262844 0.055407614 -1.84366941 5.61705303 0.055407614 -1.6673553 5.61705303 -0.4872314
		 -1.20575905 5.61705303 -0.82260078 -0.6351946 5.61705303 -0.8226009 -0.17359823 5.61705303 -0.48723158
		 0.0027158856 5.61705303 0.055407614 -2.059360027 5.61705303 0.055407614 -2.059360027 6.038262844 0.055407614
		 -1.84185266 5.61705303 -0.61401123 -1.84185266 6.038262844 -0.61401123 -1.27241111 5.61705303 -1.027734876
		 -1.27241111 6.038262844 -1.027734876 -0.56854248 5.61705303 -1.027734995 -0.56854248 6.038262844 -1.027734995
		 0.00089925528 5.61705303 -0.61401141 0.00089925528 6.038262844 -0.61401141 0.21840662 5.61705303 0.055407614
		 0.21840662 6.038262844 0.055407614 -0.34786993 7.036477089 -0.36061576 -0.70176047 7.036477089 -0.61773223
		 -1.1391933 7.036477089 -0.61773211 -1.49308372 7.036477089 -0.36061564 -1.62825775 7.036477089 0.055407614
		 -0.21269579 7.036477089 0.055407614 -0.34786993 8.15208817 -0.36061594 -0.70176047 8.15208817 -0.61773247
		 -1.1391933 8.15208817 -0.61773235 -1.49308372 8.15208817 -0.36061582 -1.62825775 8.15208817 0.055407614
		 -0.21269579 8.15208817 0.055407614 -0.6648913 6.74477339 -0.73120385 -0.25134522 6.74477339 -0.4307451
		 -0.093384698 6.74477339 0.055407614 -1.74756885 6.74477339 0.055407614 -1.58960831 6.74477339 -0.43074495
		 -1.17606235 6.74477339 -0.73120379 -1.58656979 6.84707212 -0.42853719 -1.1749016 6.84707212 -0.72763157
		 -0.66605198 6.84707212 -0.72763169 -0.25438392 6.84707212 -0.42853737 -0.097140759 6.84707212 0.055407614
		 -1.74381292 6.84707212 0.055407614 -1.6673553 6.038262844 0.59804666 -1.58960831 6.74477339 0.54156017
		 -1.20575905 6.038262844 0.93341601 -1.17606235 6.74477339 0.84201902 -0.6351946 6.038262844 0.93341613
		 -0.6648913 6.74477339 0.84201908 -0.17359823 6.038262844 0.59804678 -0.25134522 6.74477339 0.54156029
		 -1.84185266 5.61705303 0.72482646 -1.84185266 6.038262844 0.72482646 -1.27241111 5.61705303 1.13855004
		 -1.27241111 6.038262844 1.13855004 -0.56854248 5.61705303 1.13855028 -0.56854248 6.038262844 1.13855028
		 0.00089925528 5.61705303 0.72482663 0.00089925528 6.038262844 0.72482663 -0.6351946 5.61705303 0.93341613
		 -1.20575905 5.61705303 0.93341601 -0.17359823 5.61705303 0.59804678 -0.25890887 7.036477089 0.53606498
		 -0.66778034 7.036477089 0.83312744 -0.70176047 7.036477089 0.72854745 -0.34786993 7.036477089 0.47143099
		 -1.17317331 7.036477089 0.83312738 -1.1391933 7.036477089 0.72854733 -1.58204472 7.036477089 0.53606486
		 -1.49308372 7.036477089 0.47143087 -0.25438392 6.84707212 0.5393526 -0.66605198 6.84707212 0.83844692
		 -1.58656979 6.84707212 0.53935242 -1.1749016 6.84707212 0.8384468 -0.34786993 8.15208817 0.47143117
		 -0.70176047 8.15208817 0.72854769 -1.1391933 8.15208817 0.72854757 -1.49308372 8.15208817 0.47143105
		 -0.17359823 4.98058414 0.59804678 -0.6351946 4.98058414 0.93341613 -1.20575905 4.98058414 0.93341601
		 -1.6673553 4.98058414 0.59804666 -1.6673553 5.61705303 0.59804666;
	setAttr -s 182 ".ed";
	setAttr ".ed[0:165]"  6 7 0 7 8 0 8 9 0 9 10 0 11 6 0 12 51 0 13 52 0 12 13 0
		 14 53 0 13 14 0 15 48 0 14 15 0 16 49 0 15 16 0 17 50 0 16 17 0 20 21 0 21 22 0 12 25 0
		 24 25 0 24 26 0 13 27 1 25 27 0 26 27 0 20 28 1 26 28 0 14 29 1 27 29 0 28 29 0 21 30 1
		 28 30 0 15 31 1 29 31 0 30 31 0 22 32 1 30 32 0 16 33 1 31 33 0 32 33 0 32 34 0 17 35 0
		 33 35 0 34 35 0 6 36 1 7 37 1 36 37 0 8 38 1 37 38 0 9 39 1 38 39 0 10 40 0 39 40 0
		 11 41 0 41 36 0 48 56 0 49 57 0 48 49 1 50 58 0 49 50 1 51 59 0 52 54 0 51 52 1 53 55 0
		 52 53 1 53 48 1 54 9 0 55 8 0 56 7 0 57 6 0 58 11 0 59 10 0 42 43 0 43 44 0 44 45 0
		 45 46 0 37 43 0 38 44 0 39 45 0 40 46 0 41 47 0 36 42 0 42 47 0 46 47 0 0 1 0 1 21 0
		 2 3 0 3 19 0 19 20 0 3 4 0 4 18 0 18 19 0 0 22 0 22 23 0 0 5 0 18 24 0 19 26 1 23 34 0
		 5 23 0 1 2 0 2 20 0 4 5 0 12 60 0 51 61 1 60 61 0 60 62 0 61 63 1 62 63 0 62 64 0
		 63 65 1 64 65 0 64 66 0 65 67 1 66 67 0 66 17 0 67 50 1 24 68 0 25 69 0 68 69 0 68 70 0
		 69 71 0 70 71 0 70 72 0 71 73 0 72 73 0 72 74 0 73 75 0 74 75 0 74 34 0 75 35 0 60 69 1
		 62 71 1 77 76 0 77 70 1 76 72 1 64 73 1 76 78 0 78 74 1 66 75 1 79 80 0 80 81 1 82 81 0
		 79 82 1 80 83 0 83 84 1 81 84 0 83 85 0 85 86 1 84 86 0 85 10 0 86 40 0 11 79 0 41 82 0
		 67 87 0 65 88 0 88 80 0 87 79 0 61 89 0 89 85 0 63 90 0 90 83 0 91 47 0 91 92 0 92 93 0
		 93 94 0 94 46 0 82 91 0;
	setAttr ".ed[166:181]" 81 92 0 84 93 0 86 94 0 95 78 0 95 96 0 96 76 0 97 77 0
		 97 98 0 98 99 0 99 77 0 98 4 0 18 99 0 95 5 0 78 23 0 96 97 0 99 68 1;
	setAttr -s 82 -ch 356 ".fc[0:81]" -type "polyFaces" 
		f 4 -8 5 61 -7
		mu 0 4 0 1 2 3
		f 4 -10 6 63 -9
		mu 0 4 4 0 3 5
		f 4 -12 8 64 -11
		mu 0 4 6 4 5 7
		f 4 -14 10 56 -13
		mu 0 4 8 6 7 9
		f 4 -16 12 58 -15
		mu 0 4 10 11 12 13
		f 4 -21 19 22 -24
		mu 0 4 14 15 16 17
		f 4 -26 23 27 -29
		mu 0 4 18 14 17 19
		f 4 -31 28 32 -34
		mu 0 4 20 18 19 21
		f 4 -36 33 37 -39
		mu 0 4 22 20 21 23
		f 4 -40 38 41 -43
		mu 0 4 24 25 26 27
		f 4 7 21 -23 -19
		mu 0 4 1 0 17 16
		f 4 9 26 -28 -22
		mu 0 4 0 4 19 17
		f 4 -17 24 30 -30
		mu 0 4 28 29 18 20
		f 4 11 31 -33 -27
		mu 0 4 4 6 21 19
		f 4 -18 29 35 -35
		mu 0 4 30 28 20 22
		f 4 13 36 -38 -32
		mu 0 4 6 8 23 21
		f 4 15 40 -42 -37
		mu 0 4 11 10 27 26
		f 4 0 44 -46 -44
		mu 0 4 31 32 33 34
		f 4 1 46 -48 -45
		mu 0 4 32 35 36 33
		f 4 2 48 -50 -47
		mu 0 4 35 37 38 36
		f 4 3 50 -52 -49
		mu 0 4 37 39 40 38
		f 4 4 43 -54 -53
		mu 0 4 41 31 34 42
		f 6 -56 -57 54 67 -1 -69
		mu 0 6 43 9 7 44 45 46
		f 6 -58 -59 55 68 -5 -70
		mu 0 6 47 13 12 48 49 50
		f 6 -61 -62 59 70 -4 -66
		mu 0 6 51 3 2 52 53 54
		f 6 -63 -64 60 65 -3 -67
		mu 0 6 55 5 3 51 54 56
		f 6 -55 -65 62 66 -2 -68
		mu 0 6 44 7 5 55 56 45
		f 6 -82 71 72 73 74 82
		mu 0 6 57 58 59 60 61 62
		f 4 -81 45 75 -72
		mu 0 4 58 34 33 59
		f 4 -76 47 76 -73
		mu 0 4 59 33 36 60
		f 4 -77 49 77 -74
		mu 0 4 60 36 38 61
		f 4 -78 51 78 -75
		mu 0 4 61 38 40 62
		f 4 -80 53 80 81
		mu 0 4 57 42 34 58
		f 4 -92 83 84 17
		mu 0 4 30 63 64 28
		f 4 -100 85 86 87
		mu 0 4 29 65 66 67
		f 4 -87 88 89 90
		mu 0 4 67 66 68 69
		f 4 -98 -94 91 92
		mu 0 4 70 71 72 73
		f 6 -84 93 -101 -89 -86 -99
		mu 0 6 74 75 76 77 78 79
		f 4 -96 -91 94 20
		mu 0 4 14 67 69 15
		f 4 -25 -88 95 25
		mu 0 4 18 29 67 14
		f 4 -97 -93 34 39
		mu 0 4 24 70 73 25
		f 4 103 -103 -6 101
		mu 0 4 80 81 82 83
		f 4 106 -106 -104 104
		mu 0 4 84 85 81 80
		f 4 109 -109 -107 107
		mu 0 4 86 87 85 84
		f 4 112 -112 -110 110
		mu 0 4 88 89 87 86
		f 4 14 -115 -113 113
		mu 0 4 90 91 92 93
		f 4 117 -117 -20 115
		mu 0 4 94 95 96 97
		f 4 120 -120 -118 118
		mu 0 4 98 99 95 94
		f 4 123 -123 -121 121
		mu 0 4 100 101 99 98
		f 4 126 -126 -124 124
		mu 0 4 102 103 101 100
		f 4 42 -129 -127 127
		mu 0 4 104 105 106 107
		f 4 18 116 -130 -102
		mu 0 4 83 96 95 80
		f 4 129 119 -131 -105
		mu 0 4 80 95 99 84
		f 4 133 -122 -133 131
		mu 0 4 108 100 98 109
		f 4 130 122 -135 -108
		mu 0 4 84 99 101 86
		f 4 136 -125 -134 135
		mu 0 4 110 102 100 108
		f 4 134 125 -138 -111
		mu 0 4 86 101 103 88
		f 4 137 128 -41 -114
		mu 0 4 93 106 105 90
		f 4 141 140 -140 -139
		mu 0 4 111 112 113 114
		f 4 139 144 -144 -143
		mu 0 4 114 113 115 116
		f 4 143 147 -147 -146
		mu 0 4 116 115 117 118
		f 4 146 149 -51 -149
		mu 0 4 118 117 119 120
		f 4 52 151 -142 -151
		mu 0 4 121 122 112 111
		f 6 155 138 -155 -154 111 152
		mu 0 6 123 124 125 126 87 89
		f 6 69 150 -156 -153 114 57
		mu 0 6 127 128 129 130 92 91
		f 6 157 148 -71 -60 102 156
		mu 0 6 131 132 133 134 82 81
		f 6 159 145 -158 -157 105 158
		mu 0 6 135 136 132 131 81 85
		f 6 154 142 -160 -159 108 153
		mu 0 6 126 125 136 135 85 87
		f 6 -83 -165 -164 -163 -162 160
		mu 0 6 137 138 139 140 141 142
		f 4 161 -167 -141 165
		mu 0 4 142 141 113 112
		f 4 162 -168 -145 166
		mu 0 4 141 140 115 113
		f 4 163 -169 -148 167
		mu 0 4 140 139 117 115
		f 4 164 -79 -150 168
		mu 0 4 139 138 119 117
		f 4 -161 -166 -152 79
		mu 0 4 137 142 112 122
		f 4 -136 -172 -171 169
		mu 0 4 110 108 143 144
		f 4 -176 -175 -174 172
		mu 0 4 109 145 146 147
		f 4 -178 -90 -177 174
		mu 0 4 145 148 149 146
		f 4 -180 -170 178 97
		mu 0 4 150 151 152 153
		f 6 180 173 176 100 -179 170
		mu 0 6 154 155 156 157 158 159
		f 4 -116 -95 177 181
		mu 0 4 94 97 148 145
		f 4 -119 -182 175 132
		mu 0 4 98 94 145 109
		f 4 -128 -137 179 96
		mu 0 4 104 107 151 150;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "claw_2" -p "crane_claw";
	setAttr ".rp" -type "double3" -0.90916359130215285 5.3998329623150232 0.74855561652999181 ;
	setAttr ".sp" -type "double3" -0.90916359130215285 5.3998329623150232 0.74855561652999181 ;
createNode mesh -n "claw_Shape2" -p "claw_2";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 96 ".uvst[0].uvsp[0:95]" -type "float2" 0.40000001 0.3125
		 0.40000001 0.42888451 0.42500001 0.42888451 0.42500001 0.3125 0.40000001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.42500001
		 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.40000001
		 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001
		 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.40000001 0.3125 0.42500001
		 0.3125 0.40000001 0.3125 0.42500001 0.3125 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451 0.42500001 0.42888451
		 0.42500001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451 0.40000001 0.42888451
		 0.42500001 0.3125 0.40000001 0.3125;
	setAttr ".uvst[1].uvsn" -type "string" "map11";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 96 ".pt[0:95]" -type "float3"  0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 
		-0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2 0 0.14198092 -0.2;
	setAttr -s 96 ".vt[0:95]"  -0.78600055 2.11380792 0.9981485 -1.076512098 2.11439848 0.9978106
		 -1.077270627 2.02619195 1.014654517 -0.78675902 2.025601149 1.014992118 -0.68187004 4.92126322 1.31555033
		 -1.15908384 4.92126322 1.31555009 -1.1124084 4.97333002 1.69768429 -0.72854537 4.97333002 1.69768453
		 -1.065733194 5.13678455 2.079818487 -0.77522081 5.13678455 2.079818487 -1.15908384 5.4535985 1.31555009
		 -1.1124084 5.4015317 1.69768429 -1.065733194 5.46085262 2.079818487 -0.68187004 5.4535985 1.31555033
		 -0.72854537 5.4015317 1.69768453 -0.77522081 5.46085262 2.079818487 -1.076661348 5.119133 2.39727116
		 -0.78631347 5.1188674 2.4070406 -1.076512098 5.089263439 2.60789943 -0.78600055 5.089936733 2.60799384
		 -1.076512098 5.0068092346 2.70368648 -0.78600055 5.007376194 2.70406222 -1.076512098 4.87423897 2.7362628
		 -0.78600055 4.87451887 2.73688269 -1.076512098 4.66625357 2.76046681 -0.78600055 4.6663518 2.76114011
		 -1.076512098 4.4365325 2.765095 -0.78600055 4.43654871 2.76577544 -1.076512098 4.22588158 2.75879145
		 -0.78600055 4.22586107 2.75947189 -1.076512098 4.024277687 2.74842906 -0.78600055 4.024241924 2.74910903
		 -1.076512098 3.81150198 2.74044251 -0.78600055 3.81147575 2.74112296 -1.076512098 3.60036922 2.72107029
		 -0.78600055 3.60030699 2.72174835 -1.076512098 3.42398596 2.65652537 -0.78600055 3.42376661 2.65717006
		 -1.076512098 3.28468132 2.52277112 -0.78600055 3.28423929 2.52328897 -1.076512098 3.14608765 2.31287599
		 -0.78600055 3.14552736 2.3132627 -1.076512098 3.0077910423 2.0656569 -0.78600055 3.0071976185 2.065989971
		 -1.076512098 2.87537479 1.83887649 -0.78600055 2.87478685 1.83921909 -1.076512098 2.74954295 1.64512229
		 -0.78600055 2.74897218 1.64549303 -1.076512098 2.5000155 1.46615696 -0.78600055 2.49944901 1.4665339
		 -1.076512098 2.31367803 1.278023 -0.78600055 2.31310081 1.27838302 -1.076512098 2.2371242 1.073794842
		 -0.78600055 2.23653769 1.074139833 -1.076957703 5.44272232 2.4148736 -1.077270627 5.41019964 2.65283465
		 -1.077270627 5.27701139 2.88259959 -1.077270627 5.0077228546 3.031561375 -1.077270627 4.7133317 3.081095934
		 -1.077270627 4.44472075 3.089058399 -1.077270627 4.21655226 3.082724094 -1.077270627 4.007707119 3.072072029
		 -1.077270627 3.79936934 3.064282179 -1.077270627 3.57110548 3.04381299 -1.077270627 3.31996632 2.96344423
		 -1.077270627 3.074559927 2.76948619 -1.077270627 2.8795948 2.49726963 -1.077270627 2.72536039 2.2245667
		 -1.077270627 2.59553456 2.0023047924 -1.077270627 2.47790623 1.82185054 -1.077270627 2.36060238 1.64592671
		 -1.077270627 2.23791647 1.44980884 -1.077270627 2.11321926 1.23833632 -0.78660971 5.44245672 2.42464328
		 -0.78675902 5.41087341 2.65292883 -0.78675902 5.27757883 2.88297534 -0.78675902 5.0080032349 3.032181263
		 -0.78675902 4.71343088 3.081768751 -0.78675902 4.44473791 3.089738369 -0.78675902 4.21653271 3.083404064
		 -0.78675902 4.0076723099 3.072751522 -0.78675902 3.79934359 3.064962149 -0.78675902 3.57104373 3.044490576
		 -0.78675902 3.31974745 2.96408844 -0.78675902 3.074118614 2.77000356 -0.78675902 2.87903547 2.49765611
		 -0.78675902 2.72476745 2.22489953 -0.78675902 2.59494686 2.0026471615 -0.78675902 2.47733545 1.8222208
		 -0.78675902 2.3600359 1.64630342 -0.78675902 2.23733902 1.45016861 -0.78675902 2.11263275 1.23868108
		 -0.6351946 5.61705303 0.93341613 -1.20575905 5.61705303 0.93341601 -1.20575905 4.98058414 0.93341601
		 -0.6351946 4.98058414 0.93341613;
	setAttr -s 188 ".ed";
	setAttr ".ed[0:165]"  0 1 0 1 2 0 2 3 0 0 3 0 4 5 1 5 6 0 7 6 1 4 7 0 6 8 0
		 9 8 1 7 9 0 5 10 1 10 11 0 6 11 1 11 12 0 8 12 1 92 13 0 10 13 1 93 10 0 13 14 0
		 11 14 1 14 15 0 12 15 1 4 13 1 7 14 1 9 15 1 8 16 0 17 16 1 9 17 0 16 18 0 19 18 0
		 17 19 0 18 20 0 21 20 0 19 21 0 20 22 0 23 22 1 21 23 0 22 24 0 25 24 1 23 25 0 24 26 0
		 27 26 1 25 27 0 26 28 0 29 28 1 27 29 0 28 30 0 31 30 1 29 31 0 30 32 0 33 32 1 31 33 0
		 32 34 0 35 34 1 33 35 0 34 36 0 37 36 1 35 37 0 36 38 0 39 38 1 37 39 0 38 40 0 41 40 1
		 39 41 0 40 42 0 43 42 1 41 43 0 42 44 0 45 44 1 43 45 0 44 46 0 47 46 1 45 47 0 46 48 0
		 49 48 1 47 49 0 48 50 0 51 50 1 49 51 0 50 52 0 53 52 1 51 53 0 52 1 0 53 0 0 12 54 0
		 16 54 1 54 55 0 18 55 1 55 56 0 20 56 1 56 57 0 22 57 1 57 58 0 24 58 1 58 59 0 26 59 1
		 59 60 0 28 60 1 60 61 0 30 61 1 61 62 0 32 62 1 62 63 0 34 63 1 63 64 0 36 64 1 64 65 0
		 38 65 1 65 66 0 40 66 1 66 67 0 42 67 1 67 68 0 44 68 1 68 69 0 46 69 1 69 70 0 48 70 1
		 70 71 0 50 71 1 71 72 0 52 72 1 72 2 0 15 73 0 54 73 1 73 74 0 55 74 1 74 75 0 56 75 0
		 75 76 0 57 76 1 76 77 0 58 77 1 77 78 0 59 78 1 78 79 0 60 79 1 79 80 0 61 80 1 80 81 0
		 62 81 1 81 82 0 63 82 1 82 83 0 64 83 1 83 84 0 65 84 1 84 85 0 66 85 1 85 86 0 67 86 1
		 86 87 0 68 87 1 87 88 0 69 88 1 88 89 0 70 89 1 89 90 0 71 90 1 90 91 0 72 91 1 91 3 0
		 17 73 1 19 74 1 21 75 1;
	setAttr ".ed[166:187]" 23 76 1 25 77 1 27 78 1 29 79 1 31 80 1 33 81 1 35 82 1
		 37 83 1 39 84 1 41 85 1 43 86 1 45 87 1 47 88 1 49 89 1 51 90 1 53 91 1 95 4 0 94 5 0
		 93 92 0 95 94 0 94 93 0 95 92 0;
	setAttr -s 93 -ch 372 ".fc[0:92]" -type "polyFaces" 
		f 4 3 -3 -2 -1
		mu 0 4 0 1 2 3
		f 4 7 6 -6 -5
		mu 0 4 4 5 6 7
		f 4 10 9 -9 -7
		mu 0 4 5 8 9 6
		f 4 5 13 -13 -12
		mu 0 4 7 6 10 11
		f 4 8 15 -15 -14
		mu 0 4 6 9 12 10
		f 4 18 17 -17 -185
		mu 0 4 13 11 14 15
		f 4 12 20 -20 -18
		mu 0 4 11 10 16 14
		f 4 14 22 -22 -21
		mu 0 4 10 12 17 16
		f 4 19 -25 -8 23
		mu 0 4 14 16 5 4
		f 4 21 -26 -11 24
		mu 0 4 16 17 8 5
		f 4 28 27 -27 -10
		mu 0 4 8 18 19 9
		f 4 31 30 -30 -28
		mu 0 4 18 20 21 19
		f 4 34 33 -33 -31
		mu 0 4 20 22 23 21
		f 4 37 36 -36 -34
		mu 0 4 22 24 25 23
		f 4 40 39 -39 -37
		mu 0 4 24 26 27 25
		f 4 43 42 -42 -40
		mu 0 4 26 28 29 27
		f 4 46 45 -45 -43
		mu 0 4 28 30 31 29
		f 4 49 48 -48 -46
		mu 0 4 30 32 33 31
		f 4 52 51 -51 -49
		mu 0 4 32 34 35 33
		f 4 55 54 -54 -52
		mu 0 4 34 36 37 35
		f 4 58 57 -57 -55
		mu 0 4 36 38 39 37
		f 4 61 60 -60 -58
		mu 0 4 38 40 41 39
		f 4 64 63 -63 -61
		mu 0 4 40 42 43 41
		f 4 67 66 -66 -64
		mu 0 4 42 44 45 43
		f 4 70 69 -69 -67
		mu 0 4 44 46 47 45
		f 4 73 72 -72 -70
		mu 0 4 46 48 49 47
		f 4 76 75 -75 -73
		mu 0 4 48 50 51 49
		f 4 79 78 -78 -76
		mu 0 4 50 52 53 51
		f 4 82 81 -81 -79
		mu 0 4 52 54 55 53
		f 4 84 0 -84 -82
		mu 0 4 54 0 3 55
		f 4 26 86 -86 -16
		mu 0 4 9 19 56 12
		f 4 29 88 -88 -87
		mu 0 4 19 21 57 56
		f 4 32 90 -90 -89
		mu 0 4 21 23 58 57
		f 4 35 92 -92 -91
		mu 0 4 23 25 59 58
		f 4 38 94 -94 -93
		mu 0 4 25 27 60 59
		f 4 41 96 -96 -95
		mu 0 4 27 29 61 60
		f 4 44 98 -98 -97
		mu 0 4 29 31 62 61
		f 4 47 100 -100 -99
		mu 0 4 31 33 63 62
		f 4 50 102 -102 -101
		mu 0 4 33 35 64 63
		f 4 53 104 -104 -103
		mu 0 4 35 37 65 64
		f 4 56 106 -106 -105
		mu 0 4 37 39 66 65
		f 4 59 108 -108 -107
		mu 0 4 39 41 67 66
		f 4 62 110 -110 -109
		mu 0 4 41 43 68 67
		f 4 65 112 -112 -111
		mu 0 4 43 45 69 68
		f 4 68 114 -114 -113
		mu 0 4 45 47 70 69
		f 4 71 116 -116 -115
		mu 0 4 47 49 71 70
		f 4 74 118 -118 -117
		mu 0 4 49 51 72 71
		f 4 77 120 -120 -119
		mu 0 4 51 53 73 72
		f 4 80 122 -122 -121
		mu 0 4 53 55 74 73
		f 4 83 1 -124 -123
		mu 0 4 55 3 2 74
		f 4 85 125 -125 -23
		mu 0 4 12 56 75 17
		f 4 87 127 -127 -126
		mu 0 4 56 57 76 75
		f 4 89 129 -129 -128
		mu 0 4 57 58 77 76
		f 4 91 131 -131 -130
		mu 0 4 58 59 78 77
		f 4 93 133 -133 -132
		mu 0 4 59 60 79 78
		f 4 95 135 -135 -134
		mu 0 4 60 61 80 79
		f 4 97 137 -137 -136
		mu 0 4 61 62 81 80
		f 4 99 139 -139 -138
		mu 0 4 62 63 82 81
		f 4 101 141 -141 -140
		mu 0 4 63 64 83 82
		f 4 103 143 -143 -142
		mu 0 4 64 65 84 83
		f 4 105 145 -145 -144
		mu 0 4 65 66 85 84
		f 4 107 147 -147 -146
		mu 0 4 66 67 86 85
		f 4 109 149 -149 -148
		mu 0 4 67 68 87 86
		f 4 111 151 -151 -150
		mu 0 4 68 69 88 87
		f 4 113 153 -153 -152
		mu 0 4 69 70 89 88
		f 4 115 155 -155 -154
		mu 0 4 70 71 90 89
		f 4 117 157 -157 -156
		mu 0 4 71 72 91 90
		f 4 119 159 -159 -158
		mu 0 4 72 73 92 91
		f 4 121 161 -161 -160
		mu 0 4 73 74 93 92
		f 4 123 2 -163 -162
		mu 0 4 74 2 1 93
		f 4 124 -164 -29 25
		mu 0 4 17 75 18 8
		f 4 126 -165 -32 163
		mu 0 4 75 76 20 18
		f 4 128 -166 -35 164
		mu 0 4 76 77 22 20
		f 4 130 -167 -38 165
		mu 0 4 77 78 24 22
		f 4 132 -168 -41 166
		mu 0 4 78 79 26 24
		f 4 134 -169 -44 167
		mu 0 4 79 80 28 26
		f 4 136 -170 -47 168
		mu 0 4 80 81 30 28
		f 4 138 -171 -50 169
		mu 0 4 81 82 32 30
		f 4 140 -172 -53 170
		mu 0 4 82 83 34 32
		f 4 142 -173 -56 171
		mu 0 4 83 84 36 34
		f 4 144 -174 -59 172
		mu 0 4 84 85 38 36
		f 4 146 -175 -62 173
		mu 0 4 85 86 40 38
		f 4 148 -176 -65 174
		mu 0 4 86 87 42 40
		f 4 150 -177 -68 175
		mu 0 4 87 88 44 42
		f 4 152 -178 -71 176
		mu 0 4 88 89 46 44
		f 4 154 -179 -74 177
		mu 0 4 89 90 48 46
		f 4 156 -180 -77 178
		mu 0 4 90 91 50 48
		f 4 158 -181 -80 179
		mu 0 4 91 92 52 50
		f 4 160 -182 -83 180
		mu 0 4 92 93 54 52
		f 4 162 -4 -85 181
		mu 0 4 93 1 0 54
		f 4 4 -184 -186 182
		mu 0 4 4 7 94 95
		f 4 183 11 -19 -187
		mu 0 4 94 7 11 13
		f 4 -24 -183 187 16
		mu 0 4 14 4 95 15;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "control_arms" -p "control_arm_assembly";
	setAttr ".rp" -type "double3" 0.52288371322781069 13.759995607685143 0.096307003607841501 ;
	setAttr ".sp" -type "double3" 0.52288371322781069 13.759995607685143 0.096307003607841501 ;
createNode transform -n "control_arm" -p "control_arms";
	setAttr ".t" -type "double3" -0.045026036365233502 13.759995607685143 0.096307003607841501 ;
createNode mesh -n "control_armShape" -p "control_arm";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.11502507 -0.14601529 4.32033253 0.11502507 -0.14601529 4.32033253
		 -0.11502507 0.14601529 4.32033253 0.11502507 0.14601529 4.32033253 -0.11502507 0.14601529 -4.32033253
		 0.11502507 0.14601529 -4.32033253 -0.11502507 -0.14601529 -4.32033253 0.11502507 -0.14601529 -4.32033253;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "control_arm1" -p "control_arms";
	setAttr ".t" -type "double3" 1.0907934628208549 13.759995607685143 0.096307003607841501 ;
createNode mesh -n "control_arm1Shape" -p "control_arm1";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 14 ".uvst[0].uvsp[0:13]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 8 ".vt[0:7]"  -0.11502507 -0.14601529 4.32033253 0.11502507 -0.14601529 4.32033253
		 -0.11502507 0.14601529 4.32033253 0.11502507 0.14601529 4.32033253 -0.11502507 0.14601529 -4.32033253
		 0.11502507 0.14601529 -4.32033253 -0.11502507 -0.14601529 -4.32033253 0.11502507 -0.14601529 -4.32033253;
	setAttr -s 12 ".ed[0:11]"  0 1 0 2 3 0 4 5 0 6 7 0 0 2 0 1 3 0 2 4 0
		 3 5 0 4 6 0 5 7 0 6 0 0 7 1 0;
	setAttr -s 6 -ch 24 ".fc[0:5]" -type "polyFaces" 
		f 4 0 5 -2 -5
		mu 0 4 0 1 3 2
		f 4 1 7 -3 -7
		mu 0 4 2 3 5 4
		f 4 2 9 -4 -9
		mu 0 4 4 5 7 6
		f 4 3 11 -1 -11
		mu 0 4 6 7 9 8
		f 4 -12 -10 -8 -6
		mu 0 4 1 10 11 3
		f 4 10 4 6 8
		mu 0 4 12 0 2 13;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "control_panel_assembly" -p "crane_game";
	setAttr ".rp" -type "double3" 4.908629822437347 4.4723244219726599 0.054802950432338293 ;
	setAttr ".sp" -type "double3" 4.908629822437347 4.4723244219726599 0.054802950432338293 ;
createNode transform -n "control_panel_box" -p "control_panel_assembly";
	setAttr ".t" -type "double3" 4.908629822437347 5.082348530793575 0.054802950432338293 ;
	setAttr ".r" -type "double3" -4.110621609092848e-17 0 1.1052712186527355e-16 ;
	setAttr ".s" -type "double3" 0.76753025382568041 0.90672722322466415 1.1465573894937351 ;
createNode mesh -n "control_panel_boxShape" -p "control_panel_box";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 48 ".uvst[0].uvsp[0:47]" -type "float2" 0.375 0 0.625 0 0.375
		 0.25 0.625 0.25 0.375 0.5 0.625 0.5 0.375 0.75 0.625 0.75 0.375 1 0.625 1 0.875 0
		 0.875 0.25 0.125 0 0.125 0.25 0.6149652 0.25 0.6149652 0.5 0.6149652 0.75 0.6149652
		 0 0.6149652 1 0.38581854 0.5 0.38581854 0.75 0.38581854 0 0.38581854 1 0.38581854
		 0.25 0.6149652 0.49527746 0.38581854 0.49527746 0.12972254 0.25 0.375 0.49527746
		 0.12972254 0 0.375 0.75472254 0.38581854 0.75472254 0.6149652 0.75472254 0.625 0.75472254
		 0.87027752 0 0.625 0.49527746 0.87027752 0.25 0.61496526 0.25571623 0.38581857 0.25571623
		 0.3692838 0.25 0.37500003 0.25571623 0.3692838 0 0.375 0.9942838 0.38581854 0.9942838
		 0.6149652 0.9942838 0.625 0.9942838 0.6307162 0 0.625 0.25571623 0.63071626 0.25;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 32 ".vt[0:31]"  -0.91086131 -2.64336753 0.73421925 0.91086131 -2.11692405 0.73421925
		 -0.91086131 0.54610682 0.73421925 0.91086131 0.18067285 0.73421925 -0.91086131 0.54610682 -0.73421925
		 0.91086131 0.18067285 -0.73421925 -0.91086131 -2.64336753 -0.73421925 0.91086131 -2.11692405 -0.73421925
		 0.83773839 0.19534117 0.73421925 0.83773839 0.19534117 -0.73421925 0.83773839 -2.13805509 -0.73421925
		 0.83773839 -2.13805509 0.73421925 -0.83202779 0.53029293 -0.73421931 -0.83202779 -2.62058616 -0.73421931
		 -0.83202779 -2.62058616 0.73421931 -0.83202779 0.53029293 0.73421931 0.83773839 0.19534117 -0.70648021
		 -0.83202779 0.53029293 -0.70648026 -0.91086131 0.54610682 -0.70648021 -0.91086131 -2.64336753 -0.70648021
		 -0.83202779 -2.62058616 -0.70648026 0.83773839 -2.13805509 -0.70648021 0.91086131 -2.11692405 -0.70648021
		 0.91086131 0.18067285 -0.70648021 0.83773839 0.19534117 0.70064354 -0.83202779 0.53029293 0.7006436
		 -0.91086137 0.54610682 0.70064354 -0.91086131 -2.64336753 0.7006436 -0.83202779 -2.62058616 0.70064366
		 0.83773839 -2.13805509 0.7006436 0.91086131 -2.11692405 0.7006436 0.91086137 0.18067285 0.70064354;
	setAttr -s 60 ".ed[0:59]"  0 14 0 2 15 0 4 12 0 6 13 0 0 2 0 1 3 0 2 26 0
		 3 31 0 4 6 0 5 7 0 6 19 0 7 22 0 8 3 0 9 5 0 8 24 1 10 7 0 9 10 1 11 1 0 10 21 1
		 11 8 1 12 9 0 13 10 0 12 13 1 14 11 0 13 20 1 15 8 0 14 15 1 15 25 1 16 9 1 17 12 1
		 16 17 1 18 4 0 17 18 1 19 27 0 18 19 1 20 28 1 19 20 1 21 29 1 20 21 1 22 30 0 21 22 1
		 23 5 0 22 23 1 23 16 1 24 16 1 25 17 1 24 25 1 26 18 0 25 26 1 27 0 0 26 27 1 28 14 1
		 27 28 1 29 11 1 28 29 1 30 1 0 29 30 1 31 23 0 30 31 1 31 24 1;
	setAttr -s 30 -ch 120 ".fc[0:29]" -type "polyFaces" 
		f 4 0 26 -2 -5
		mu 0 4 0 21 23 2
		f 4 1 27 48 -7
		mu 0 4 2 23 37 39
		f 4 2 22 -4 -9
		mu 0 4 4 19 20 6
		f 4 52 51 -1 -50
		mu 0 4 41 42 22 8
		f 4 -56 58 -8 -6
		mu 0 4 1 45 47 3
		f 4 49 4 6 50
		mu 0 4 40 0 2 38
		f 4 12 7 59 -15
		mu 0 4 14 3 46 36
		f 4 -17 13 9 -16
		mu 0 4 16 15 5 7
		f 4 -54 56 55 -18
		mu 0 4 18 43 44 9
		f 4 -20 17 5 -13
		mu 0 4 14 17 1 3
		f 4 20 16 -22 -23
		mu 0 4 19 15 16 20
		f 4 -52 54 53 -24
		mu 0 4 22 42 43 18
		f 4 -27 23 19 -26
		mu 0 4 23 21 17 14
		f 4 -28 25 14 46
		mu 0 4 37 23 14 36
		f 4 -30 -31 28 -21
		mu 0 4 19 25 24 15
		f 4 -33 29 -3 -32
		mu 0 4 27 25 19 4
		f 4 10 -35 31 8
		mu 0 4 12 28 26 13
		f 4 3 24 -37 -11
		mu 0 4 6 20 30 29
		f 4 -39 -25 21 18
		mu 0 4 31 30 20 16
		f 4 -41 -19 15 11
		mu 0 4 32 31 16 7
		f 4 -43 -12 -10 -42
		mu 0 4 35 33 10 11
		f 4 -44 41 -14 -29
		mu 0 4 24 34 5 15
		f 4 -46 -47 44 30
		mu 0 4 25 37 36 24
		f 4 -49 45 32 -48
		mu 0 4 39 37 25 27
		f 4 33 -51 47 34
		mu 0 4 28 40 38 26
		f 4 36 35 -53 -34
		mu 0 4 29 30 42 41
		f 4 -55 -36 38 37
		mu 0 4 43 42 30 31
		f 4 -57 -38 40 39
		mu 0 4 44 43 31 32
		f 4 -59 -40 42 -58
		mu 0 4 47 45 33 35
		f 4 -60 57 43 -45
		mu 0 4 36 46 34 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "button" -p "control_panel_assembly";
	setAttr ".t" -type "double3" 4.8774335750588564 5.4503437718021459 -0.29069175074302994 ;
	setAttr ".r" -type "double3" 0.20614189903210212 -0.57486668789174278 -13.363545619413806 ;
	setAttr ".s" -type "double3" 0.061628503197602824 0.061628503197602824 0.061628503197602824 ;
createNode mesh -n "buttonShape" -p "button";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 102 ".uvst[0].uvsp[0:101]" -type "float2" 0.64860266 0.10796607
		 0.62640899 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-08
		 0.45171607 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608
		 0.34374997 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893
		 0.4517161 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893
		 0.24809146 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998
		 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375 0.6486026 0.89203393 0.62640893 0.93559146
		 0.59184146 0.97015893 0.54828387 0.9923526 0.5 1 0.4517161 0.9923526 0.40815854 0.97015893
		 0.37359107 0.93559146 0.3513974 0.89203393 0.34374997 0.84375 0.3513974 0.79546607
		 0.37359107 0.75190854 0.40815851 0.71734107 0.45171607 0.69514734 0.5 0.68749994
		 0.54828393 0.69514734 0.59184152 0.71734101 0.62640899 0.75190848 0.64860266 0.79546607
		 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 60 ".vt[0:59]"  1.66770935 -0.17842102 -0.54187202 1.41864395 -0.17842865 -1.03070116
		 1.030700684 -0.17842102 -1.41863823 0.54187012 -0.17842102 -1.6677084 3.8146973e-06 -0.17842102 -1.75353289
		 -0.5418663 -0.17842102 -1.6677084 -1.030696869 -0.17841339 -1.41863775 -1.41863632 -0.17842865 -1.030700684
		 -1.66770554 -0.17842102 -0.54187155 -1.75352859 -0.17842102 -4.7683716e-07 -1.66770172 -0.17842865 0.54187107
		 -1.41862869 -0.17842102 1.03069973 -1.030700684 -0.17841339 1.4186368 -0.5418663 -0.17842102 1.66770744
		 3.8146973e-06 -0.17842865 1.75353169 0.54187393 -0.17842102 1.66770768 1.030700684 -0.17842102 1.4186368
		 1.41864014 -0.17842102 1.03069973 1.66770935 -0.17842102 0.54187059 1.75353241 -0.17842865 -4.7683716e-07
		 1.66771317 0.17841339 -0.54187202 1.41864014 0.17841339 -1.03070116 1.030708313 0.17841339 -1.41863823
		 0.54187393 0.17841339 -1.66770887 3.8146973e-06 0.17842102 -1.75353289 -0.54187393 0.17841339 -1.66770887
		 -1.030696869 0.17841339 -1.41863823 -1.41862869 0.17841339 -1.030700684 -1.66770554 0.17842865 -0.54187155
		 -1.75352859 0.17842865 0 -1.66770554 0.17842102 0.54187059 -1.41862869 0.17842102 1.03069973
		 -1.03068924 0.17841339 1.4186368 -0.5418663 0.17842102 1.66770768 3.8146973e-06 0.17842102 1.75353169
		 0.54187775 0.17842102 1.66770744 1.030704498 0.17841339 1.41863632 1.41863632 0.17842865 1.03069973
		 1.66771317 0.17841339 0.54187107 1.75353622 0.17841339 0 1.47331238 0.34480286 -0.46353722
		 1.25344849 0.34480286 -0.88114882 0.91089249 0.3447876 -1.21250725 0.47917557 0.34480286 -1.42517662
		 0.00054550171 0.34479523 -1.49834013 -0.47812653 0.3447876 -1.42483616 -0.91000748 0.34479523 -1.21185923
		 -1.25279236 0.3447876 -0.88025665 -1.47295761 0.34480286 -0.4624896 -1.54895401 0.3447876 0.00055027008
		 -1.47330093 0.3447876 0.46353531 -1.25344467 0.34480286 0.88114691 -0.91088867 0.34479523 1.21250486
		 -0.47917938 0.34480286 1.42517471 -0.00054550171 0.34479523 1.4983387 0.47812653 0.34479523 1.42483377
		 0.91000366 0.34479523 1.21185684 1.25280762 0.34480286 0.88025475 1.47297668 0.3447876 0.46248722
		 1.5489502 0.3447876 -0.00055217743;
	setAttr -s 100 ".ed[0:99]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 0 20 1 1 21 1 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1
		 12 32 1 13 33 1 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1 20 40 1 21 41 1 40 41 0
		 22 42 1 41 42 0 23 43 1 42 43 0 24 44 1 43 44 0 25 45 1 44 45 0 26 46 1 45 46 0 27 47 1
		 46 47 0 28 48 1 47 48 0 29 49 1 48 49 0 30 50 1 49 50 0 31 51 1 50 51 0 32 52 1 51 52 0
		 33 53 1 52 53 0 34 54 1 53 54 0 35 55 1 54 55 0 36 56 1 55 56 0 37 57 1 56 57 0 38 58 1
		 57 58 0 39 59 1 58 59 0 59 40 0;
	setAttr -s 42 -ch 200 ".fc[0:41]" -type "polyFaces" 
		f 4 0 41 -21 -41
		mu 0 4 20 21 42 41
		f 4 1 42 -22 -42
		mu 0 4 21 22 43 42
		f 4 2 43 -23 -43
		mu 0 4 22 23 44 43
		f 4 3 44 -24 -44
		mu 0 4 23 24 45 44
		f 4 4 45 -25 -45
		mu 0 4 24 25 46 45
		f 4 5 46 -26 -46
		mu 0 4 25 26 47 46
		f 4 6 47 -27 -47
		mu 0 4 26 27 48 47
		f 4 7 48 -28 -48
		mu 0 4 27 28 49 48
		f 4 8 49 -29 -49
		mu 0 4 28 29 50 49
		f 4 9 50 -30 -50
		mu 0 4 29 30 51 50
		f 4 10 51 -31 -51
		mu 0 4 30 31 52 51
		f 4 11 52 -32 -52
		mu 0 4 31 32 53 52
		f 4 12 53 -33 -53
		mu 0 4 32 33 54 53
		f 4 13 54 -34 -54
		mu 0 4 33 34 55 54
		f 4 14 55 -35 -55
		mu 0 4 34 35 56 55
		f 4 15 56 -36 -56
		mu 0 4 35 36 57 56
		f 4 16 57 -37 -57
		mu 0 4 36 37 58 57
		f 4 17 58 -38 -58
		mu 0 4 37 38 59 58
		f 4 18 59 -39 -59
		mu 0 4 38 39 60 59
		f 4 19 40 -40 -60
		mu 0 4 39 40 61 60
		f 20 -20 -19 -18 -17 -16 -15 -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1
		mu 0 20 0 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
		f 20 62 64 66 68 70 72 74 76 78 80 82 84 86 88 90 92 94 96 98 99
		mu 0 20 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101
		f 4 20 61 -63 -61
		mu 0 4 80 79 83 82
		f 4 21 63 -65 -62
		mu 0 4 79 78 84 83
		f 4 22 65 -67 -64
		mu 0 4 78 77 85 84
		f 4 23 67 -69 -66
		mu 0 4 77 76 86 85
		f 4 24 69 -71 -68
		mu 0 4 76 75 87 86
		f 4 25 71 -73 -70
		mu 0 4 75 74 88 87
		f 4 26 73 -75 -72
		mu 0 4 74 73 89 88
		f 4 27 75 -77 -74
		mu 0 4 73 72 90 89
		f 4 28 77 -79 -76
		mu 0 4 72 71 91 90
		f 4 29 79 -81 -78
		mu 0 4 71 70 92 91
		f 4 30 81 -83 -80
		mu 0 4 70 69 93 92
		f 4 31 83 -85 -82
		mu 0 4 69 68 94 93
		f 4 32 85 -87 -84
		mu 0 4 68 67 95 94
		f 4 33 87 -89 -86
		mu 0 4 67 66 96 95
		f 4 34 89 -91 -88
		mu 0 4 66 65 97 96
		f 4 35 91 -93 -90
		mu 0 4 65 64 98 97
		f 4 36 93 -95 -92
		mu 0 4 64 63 99 98
		f 4 37 95 -97 -94
		mu 0 4 63 62 100 99
		f 4 38 97 -99 -96
		mu 0 4 62 81 101 100
		f 4 39 60 -100 -98
		mu 0 4 81 80 82 101;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "button_base" -p "control_panel_assembly";
	setAttr ".t" -type "double3" 4.8774335750588564 5.4319567936777204 -0.29069175074302994 ;
	setAttr ".r" -type "double3" 0.20614189903210212 -0.57486668789174278 -13.363545619413806 ;
	setAttr ".s" -type "double3" 0.074055285356223424 0.050023494569319767 0.074055285356223424 ;
createNode mesh -n "button_baseShape" -p "button_base";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 82 ".uvst[0].uvsp[0:81]" -type "float2" 0.64860266 0.10796607
		 0.62640899 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-08
		 0.45171607 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608
		 0.34374997 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893
		 0.4517161 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893
		 0.24809146 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998
		 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt[20:39]" -type "float3"  0 7.4505806e-08 7.4505806e-09 
		-2.9802322e-08 7.4505806e-08 0 -1.4901161e-08 7.4505806e-08 -2.9802322e-08 -1.4901161e-08 
		7.4505806e-08 -2.9802322e-08 0 7.4505806e-08 -5.9604645e-08 7.4505806e-09 7.4505806e-08 
		-2.9802322e-08 -1.4901161e-08 7.4505806e-08 -2.9802322e-08 0 7.4505806e-08 0 0 7.4505806e-08 
		-7.4505806e-09 -2.9802322e-08 7.4505806e-08 0 0 7.4505806e-08 7.4505806e-09 2.9802322e-08 
		7.4505806e-08 -4.4703484e-08 -1.4901161e-08 7.4505806e-08 2.9802322e-08 0 7.4505806e-08 
		2.9802322e-08 0 7.4505806e-08 0 0 7.4505806e-08 0 1.4901161e-08 7.4505806e-08 0 0 
		7.4505806e-08 -4.4703484e-08 0 7.4505806e-08 0 2.9802322e-08 7.4505806e-08 0;
	setAttr -s 40 ".vt[0:39]"  1.66770887 -0.17842032 -0.54187143 1.41863787 -0.17842032 -1.030700684
		 1.030700684 -0.17842032 -1.41863775 0.54187137 -0.17842032 -1.66770864 0 -0.17842032 -1.75353253
		 -0.54187137 -0.17842032 -1.66770852 -1.030700445 -0.17842032 -1.41863751 -1.41863739 -0.17842032 -1.030700326
		 -1.66770816 -0.17842032 -0.54187119 -1.75353217 -0.17842032 0 -1.66770816 -0.17842032 0.54187119
		 -1.41863728 -0.17842032 1.030700207 -1.030700207 -0.17842032 1.41863716 -0.54187119 -0.17842032 1.66770804
		 -5.2259317e-08 -0.17842032 1.75353193 0.54187107 -0.17842032 1.66770792 1.030700088 -0.17842032 1.41863704
		 1.41863692 -0.17842032 1.030700207 1.6677078 -0.17842032 0.54187113 1.75353169 -0.17842032 0
		 1.66770887 0.17842032 -0.54187143 1.41863787 0.17842032 -1.030700684 1.030700684 0.17842032 -1.41863775
		 0.54187137 0.17842032 -1.66770864 0 0.17842032 -1.75353265 -0.54187137 0.17842032 -1.66770852
		 -1.030700445 0.17842032 -1.41863751 -1.41863739 0.17842032 -1.030700326 -1.66770816 0.17842032 -0.54187119
		 -1.75353217 0.17842032 0 -1.66770816 0.17842032 0.54187119 -1.41863728 0.17842032 1.030700207
		 -1.030700207 0.17842032 1.41863716 -0.54187119 0.17842032 1.66770804 -5.2259313e-08 0.17842032 1.75353193
		 0.54187107 0.17842032 1.66770792 1.030700088 0.17842032 1.41863704 1.41863692 0.17842032 1.030700207
		 1.6677078 0.17842032 0.54187113 1.75353169 0.17842032 0;
	setAttr -s 60 ".ed[0:59]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 0 20 1 1 21 1 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1
		 12 32 1 13 33 1 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1;
	setAttr -s 22 -ch 120 ".fc[0:21]" -type "polyFaces" 
		f 4 0 41 -21 -41
		mu 0 4 20 21 42 41
		f 4 1 42 -22 -42
		mu 0 4 21 22 43 42
		f 4 2 43 -23 -43
		mu 0 4 22 23 44 43
		f 4 3 44 -24 -44
		mu 0 4 23 24 45 44
		f 4 4 45 -25 -45
		mu 0 4 24 25 46 45
		f 4 5 46 -26 -46
		mu 0 4 25 26 47 46
		f 4 6 47 -27 -47
		mu 0 4 26 27 48 47
		f 4 7 48 -28 -48
		mu 0 4 27 28 49 48
		f 4 8 49 -29 -49
		mu 0 4 28 29 50 49
		f 4 9 50 -30 -50
		mu 0 4 29 30 51 50
		f 4 10 51 -31 -51
		mu 0 4 30 31 52 51
		f 4 11 52 -32 -52
		mu 0 4 31 32 53 52
		f 4 12 53 -33 -53
		mu 0 4 32 33 54 53
		f 4 13 54 -34 -54
		mu 0 4 33 34 55 54
		f 4 14 55 -35 -55
		mu 0 4 34 35 56 55
		f 4 15 56 -36 -56
		mu 0 4 35 36 57 56
		f 4 16 57 -37 -57
		mu 0 4 36 37 58 57
		f 4 17 58 -38 -58
		mu 0 4 37 38 59 58
		f 4 18 59 -39 -59
		mu 0 4 38 39 60 59
		f 4 19 40 -40 -60
		mu 0 4 39 40 61 60
		f 20 -20 -19 -18 -17 -16 -15 -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1
		mu 0 20 0 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
		f 20 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
		mu 0 20 80 79 78 77 76 75 74 73 72 71 70 69 68 67 66 65 64 63 62 81;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "joystick_assembly" -p "control_panel_assembly";
createNode transform -n "joystick_knob" -p "joystick_assembly";
	setAttr ".t" -type "double3" 4.8813668308638185 5.5934870156724745 0.44969479568932302 ;
	setAttr ".r" -type "double3" -9.6324199052723799e-18 1.5411871848435808e-16 7.0060804154754573e-16 ;
	setAttr ".s" -type "double3" 0.16122690152952202 0.16122690152952202 0.16122690152952196 ;
createNode mesh -n "joystick_knobShape" -p "joystick_knob";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 50 ".uvst[0].uvsp[0:49]" -type "float2" 0.63531649 0.078125
		 0.578125 0.020933539 0.5 0 0.421875 0.020933539 0.36468354 0.078125 0.34375 0.15625
		 0.36468354 0.234375 0.421875 0.29156646 0.5 0.3125 0.578125 0.29156646 0.63531649
		 0.234375 0.65625 0.15625 0.375 0.3125 0.39583334 0.3125 0.41666669 0.3125 0.43750003
		 0.3125 0.45833337 0.3125 0.47916672 0.3125 0.50000006 0.3125 0.52083337 0.3125 0.54166669
		 0.3125 0.5625 0.3125 0.58333331 0.3125 0.60416663 0.3125 0.62499994 0.3125 0.375
		 0.68843985 0.39583334 0.68843985 0.41666669 0.68843985 0.43750003 0.68843985 0.45833337
		 0.68843985 0.47916672 0.68843985 0.50000006 0.68843985 0.52083337 0.68843985 0.54166669
		 0.68843985 0.5625 0.68843985 0.58333331 0.68843985 0.60416663 0.68843985 0.62499994
		 0.68843985 0.63531649 0.765625 0.578125 0.70843351 0.5 0.6875 0.421875 0.70843351
		 0.36468354 0.765625 0.34375 0.84375 0.36468354 0.921875 0.421875 0.97906649 0.5 1
		 0.578125 0.97906649 0.63531649 0.921875 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 24 ".vt[0:23]"  0.3258062 -1.31163597 -0.1881043 0.1881043 -1.31163597 -0.3258062
		 0 -1.31163597 -0.3762086 -0.1881043 -1.31163597 -0.3258062 -0.3258062 -1.31163597 -0.1881043
		 -0.3762086 -1.31163597 0 -0.3258062 -1.31163597 0.1881043 -0.1881043 -1.31163597 0.3258062
		 0 -1.31163597 0.3762086 0.1881043 -1.31163597 0.3258062 0.3258062 -1.31163597 0.1881043
		 0.3762086 -1.31163597 0 0.3258062 1.31163597 -0.1881043 0.1881043 1.31163597 -0.3258062
		 0 1.31163597 -0.3762086 -0.1881043 1.31163597 -0.3258062 -0.3258062 1.31163597 -0.1881043
		 -0.3762086 1.31163597 0 -0.3258062 1.31163597 0.1881043 -0.1881043 1.31163597 0.3258062
		 0 1.31163597 0.3762086 0.1881043 1.31163597 0.3258062 0.3258062 1.31163597 0.1881043
		 0.3762086 1.31163597 0;
	setAttr -s 36 ".ed[0:35]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 0 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 12 0 0 12 0 1 13 0 2 14 0 3 15 0 4 16 0
		 5 17 0 6 18 0 7 19 0 8 20 0 9 21 0 10 22 0 11 23 0;
	setAttr -s 14 -ch 72 ".fc[0:13]" -type "polyFaces" 
		f 4 0 25 -13 -25
		mu 0 4 12 13 26 25
		f 4 1 26 -14 -26
		mu 0 4 13 14 27 26
		f 4 2 27 -15 -27
		mu 0 4 14 15 28 27
		f 4 3 28 -16 -28
		mu 0 4 15 16 29 28
		f 4 4 29 -17 -29
		mu 0 4 16 17 30 29
		f 4 5 30 -18 -30
		mu 0 4 17 18 31 30
		f 4 6 31 -19 -31
		mu 0 4 18 19 32 31
		f 4 7 32 -20 -32
		mu 0 4 19 20 33 32
		f 4 8 33 -21 -33
		mu 0 4 20 21 34 33
		f 4 9 34 -22 -34
		mu 0 4 21 22 35 34
		f 4 10 35 -23 -35
		mu 0 4 22 23 36 35
		f 4 11 24 -24 -36
		mu 0 4 23 24 37 36
		f 12 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1
		mu 0 12 0 11 10 9 8 7 6 5 4 3 2 1
		f 12 12 13 14 15 16 17 18 19 20 21 22 23
		mu 0 12 48 47 46 45 44 43 42 41 40 39 38 49;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "joystick_pole" -p "joystick_assembly";
	setAttr ".t" -type "double3" 4.8774687972898088 5.8982912359284647 0.44460854984814918 ;
	setAttr ".s" -type "double3" 0.16295908465827202 0.16295908465827202 0.16295908465827197 ;
createNode mesh -n "joystick_poleShape" -p "joystick_pole";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 79 ".uvst[0].uvsp[0:78]" -type "float2" 0 0.125 0.125 0.125
		 0.25 0.125 0.375 0.125 0.5 0.125 0.625 0.125 0.75 0.125 0.875 0.125 1 0.125 0 0.25
		 0.125 0.25 0.25 0.25 0.375 0.25 0.5 0.25 0.625 0.25 0.75 0.25 0.875 0.25 1 0.25 0
		 0.375 0.125 0.375 0.25 0.375 0.375 0.375 0.5 0.375 0.625 0.375 0.75 0.375 0.875 0.375
		 1 0.375 0 0.5 0.125 0.5 0.25 0.5 0.375 0.5 0.5 0.5 0.625 0.5 0.75 0.5 0.875 0.5 1
		 0.5 0 0.625 0.125 0.625 0.25 0.625 0.375 0.625 0.5 0.625 0.625 0.625 0.75 0.625 0.875
		 0.625 1 0.625 0 0.75 0.125 0.75 0.25 0.75 0.375 0.75 0.5 0.75 0.625 0.75 0.75 0.75
		 0.875 0.75 1 0.75 0 0.875 0.125 0.875 0.25 0.875 0.375 0.875 0.5 0.875 0.625 0.875
		 0.75 0.875 0.875 0.875 1 0.875 0.0625 0 0.1875 0 0.3125 0 0.4375 0 0.5625 0 0.6875
		 0 0.8125 0 0.9375 0 0.0625 1 0.1875 1 0.3125 1 0.4375 1 0.5625 1 0.6875 1 0.8125
		 1 0.9375 1;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 31 ".pt";
	setAttr ".pt[19]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[25]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[26]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[27]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[28]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[29]" -type "float3" 0 5.9604645e-08 0 ;
	setAttr ".pt[32]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[33]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[34]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[35]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[36]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[37]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[38]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[39]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[40]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[41]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[42]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[43]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[44]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[45]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[46]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[47]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[48]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[49]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[50]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[51]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[52]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[53]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[54]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[55]" -type "float3" 0 1.1368468 0 ;
	setAttr ".pt[57]" -type "float3" 0 1.1368468 0 ;
	setAttr -s 58 ".vt[0:57]"  0.29152694 -0.99533522 -0.29152694 0 -0.99533522 -0.41228133
		 -0.29152694 -0.99533522 -0.29152694 -0.41228133 -0.99533522 0 -0.29152694 -0.99533522 0.29152694
		 0 -0.99533522 0.41228136 0.29152697 -0.99533522 0.29152697 0.41228139 -0.99533522 0
		 0.53867149 -0.76179659 -0.53867149 0 -0.76179659 -0.76179647 -0.53867149 -0.76179659 -0.53867149
		 -0.76179647 -0.76179659 0 -0.53867149 -0.76179659 0.53867149 0 -0.76179659 0.76179653
		 0.53867155 -0.76179659 0.53867155 0.76179659 -0.76179659 0 0.70380819 -0.41228136 -0.70380819
		 0 -0.41228136 -0.9953351 -0.70380819 -0.41228136 -0.70380819 -0.9953351 -0.41228136 0
		 -0.70380819 -0.41228136 0.70380819 0 -0.41228136 0.99533516 0.70380825 -0.41228136 0.70380825
		 0.99533522 -0.41228136 0 0.76179653 0 -0.76179653 0 0 -1.077342987 -0.76179653 0 -0.76179653
		 -1.077342987 0 0 -0.76179653 0 0.76179653 0 0 1.077342987 0.76179659 0 0.76179659
		 1.077343106 0 0 0.70380819 0.41228136 -0.70380819 0 0.41228136 -0.9953351 -0.70380819 0.41228136 -0.70380819
		 -0.9953351 0.41228136 0 -0.70380819 0.41228136 0.70380819 0 0.41228136 0.99533516
		 0.70380825 0.41228136 0.70380825 0.99533522 0.41228136 0 0.53867149 0.76179659 -0.53867149
		 0 0.76179659 -0.76179647 -0.53867149 0.76179659 -0.53867149 -0.76179647 0.76179659 0
		 -0.53867149 0.76179659 0.53867149 0 0.76179659 0.76179653 0.53867155 0.76179659 0.53867155
		 0.76179659 0.76179659 0 0.29152694 0.99533522 -0.29152694 0 0.99533522 -0.41228133
		 -0.29152694 0.99533522 -0.29152694 -0.41228133 0.99533522 0 -0.29152694 0.99533522 0.29152694
		 0 0.99533522 0.41228136 0.29152697 0.99533522 0.29152697 0.41228139 0.99533522 0
		 0 -1.077343106 0 0 1.077343106 0;
	setAttr -s 120 ".ed[0:119]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 0 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 8 0 16 17 0 17 18 0
		 18 19 0 19 20 0 20 21 0 21 22 0 22 23 0 23 16 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 24 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 32 0
		 40 41 0 41 42 0 42 43 0 43 44 0 44 45 0 45 46 0 46 47 0 47 40 0 48 49 0 49 50 0 50 51 0
		 51 52 0 52 53 0 53 54 0 54 55 0 55 48 0 0 8 0 1 9 0 2 10 0 3 11 0 4 12 0 5 13 0 6 14 0
		 7 15 0 8 16 0 9 17 0 10 18 0 11 19 0 12 20 0 13 21 0 14 22 0 15 23 0 16 24 0 17 25 0
		 18 26 0 19 27 0 20 28 0 21 29 0 22 30 0 23 31 0 24 32 0 25 33 0 26 34 0 27 35 0 28 36 0
		 29 37 0 30 38 0 31 39 0 32 40 0 33 41 0 34 42 0 35 43 0 36 44 0 37 45 0 38 46 0 39 47 0
		 40 48 0 41 49 0 42 50 0 43 51 0 44 52 0 45 53 0 46 54 0 47 55 0 56 0 0 56 1 0 56 2 0
		 56 3 0 56 4 0 56 5 0 56 6 0 56 7 0 48 57 0 49 57 0 50 57 0 51 57 0 52 57 0 53 57 0
		 54 57 0 55 57 0;
	setAttr -s 64 -ch 240 ".fc[0:63]" -type "polyFaces" 
		f 4 0 57 -9 -57
		mu 0 4 0 1 10 9
		f 4 1 58 -10 -58
		mu 0 4 1 2 11 10
		f 4 2 59 -11 -59
		mu 0 4 2 3 12 11
		f 4 3 60 -12 -60
		mu 0 4 3 4 13 12
		f 4 4 61 -13 -61
		mu 0 4 4 5 14 13
		f 4 5 62 -14 -62
		mu 0 4 5 6 15 14
		f 4 6 63 -15 -63
		mu 0 4 6 7 16 15
		f 4 7 56 -16 -64
		mu 0 4 7 8 17 16
		f 4 8 65 -17 -65
		mu 0 4 9 10 19 18
		f 4 9 66 -18 -66
		mu 0 4 10 11 20 19
		f 4 10 67 -19 -67
		mu 0 4 11 12 21 20
		f 4 11 68 -20 -68
		mu 0 4 12 13 22 21
		f 4 12 69 -21 -69
		mu 0 4 13 14 23 22
		f 4 13 70 -22 -70
		mu 0 4 14 15 24 23
		f 4 14 71 -23 -71
		mu 0 4 15 16 25 24
		f 4 15 64 -24 -72
		mu 0 4 16 17 26 25
		f 4 16 73 -25 -73
		mu 0 4 18 19 28 27
		f 4 17 74 -26 -74
		mu 0 4 19 20 29 28
		f 4 18 75 -27 -75
		mu 0 4 20 21 30 29
		f 4 19 76 -28 -76
		mu 0 4 21 22 31 30
		f 4 20 77 -29 -77
		mu 0 4 22 23 32 31
		f 4 21 78 -30 -78
		mu 0 4 23 24 33 32
		f 4 22 79 -31 -79
		mu 0 4 24 25 34 33
		f 4 23 72 -32 -80
		mu 0 4 25 26 35 34
		f 4 24 81 -33 -81
		mu 0 4 27 28 37 36
		f 4 25 82 -34 -82
		mu 0 4 28 29 38 37
		f 4 26 83 -35 -83
		mu 0 4 29 30 39 38
		f 4 27 84 -36 -84
		mu 0 4 30 31 40 39
		f 4 28 85 -37 -85
		mu 0 4 31 32 41 40
		f 4 29 86 -38 -86
		mu 0 4 32 33 42 41
		f 4 30 87 -39 -87
		mu 0 4 33 34 43 42
		f 4 31 80 -40 -88
		mu 0 4 34 35 44 43
		f 4 32 89 -41 -89
		mu 0 4 36 37 46 45
		f 4 33 90 -42 -90
		mu 0 4 37 38 47 46
		f 4 34 91 -43 -91
		mu 0 4 38 39 48 47
		f 4 35 92 -44 -92
		mu 0 4 39 40 49 48
		f 4 36 93 -45 -93
		mu 0 4 40 41 50 49
		f 4 37 94 -46 -94
		mu 0 4 41 42 51 50
		f 4 38 95 -47 -95
		mu 0 4 42 43 52 51
		f 4 39 88 -48 -96
		mu 0 4 43 44 53 52
		f 4 40 97 -49 -97
		mu 0 4 45 46 55 54
		f 4 41 98 -50 -98
		mu 0 4 46 47 56 55
		f 4 42 99 -51 -99
		mu 0 4 47 48 57 56
		f 4 43 100 -52 -100
		mu 0 4 48 49 58 57
		f 4 44 101 -53 -101
		mu 0 4 49 50 59 58
		f 4 45 102 -54 -102
		mu 0 4 50 51 60 59
		f 4 46 103 -55 -103
		mu 0 4 51 52 61 60
		f 4 47 96 -56 -104
		mu 0 4 52 53 62 61
		f 3 -1 -105 105
		mu 0 3 1 0 63
		f 3 -2 -106 106
		mu 0 3 2 1 64
		f 3 -3 -107 107
		mu 0 3 3 2 65
		f 3 -4 -108 108
		mu 0 3 4 3 66
		f 3 -5 -109 109
		mu 0 3 5 4 67
		f 3 -6 -110 110
		mu 0 3 6 5 68
		f 3 -7 -111 111
		mu 0 3 7 6 69
		f 3 -8 -112 104
		mu 0 3 8 7 70
		f 3 48 113 -113
		mu 0 3 54 55 71
		f 3 49 114 -114
		mu 0 3 55 56 72
		f 3 50 115 -115
		mu 0 3 56 57 73
		f 3 51 116 -116
		mu 0 3 57 58 74
		f 3 52 117 -117
		mu 0 3 58 59 75
		f 3 53 118 -118
		mu 0 3 59 60 76
		f 3 54 119 -119
		mu 0 3 60 61 77
		f 3 55 112 -120
		mu 0 3 61 62 78;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "joystick_pole_base" -p "joystick_assembly";
	setAttr ".t" -type "double3" 4.8719705215733002 5.4231718718978099 0.45112097017392411 ;
	setAttr ".r" -type "double3" 0.20614189903210212 -0.57486668789174278 -13.363545619413806 ;
	setAttr ".s" -type "double3" 0.087642710369465956 0.050848232104165905 0.087642710369465956 ;
createNode mesh -n "joystick_pole_baseShape" -p "joystick_pole_base";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 82 ".uvst[0].uvsp[0:81]" -type "float2" 0.64860266 0.10796607
		 0.62640899 0.064408496 0.59184152 0.029841021 0.54828393 0.0076473355 0.5 -7.4505806e-08
		 0.45171607 0.0076473504 0.40815851 0.029841051 0.37359107 0.064408526 0.3513974 0.10796608
		 0.34374997 0.15625 0.3513974 0.20453392 0.37359107 0.24809146 0.40815854 0.28265893
		 0.4517161 0.3048526 0.5 0.3125 0.54828387 0.3048526 0.59184146 0.28265893 0.62640893
		 0.24809146 0.6486026 0.2045339 0.65625 0.15625 0.375 0.3125 0.38749999 0.3125 0.39999998
		 0.3125 0.41249996 0.3125 0.42499995 0.3125 0.43749994 0.3125 0.44999993 0.3125 0.46249992
		 0.3125 0.4749999 0.3125 0.48749989 0.3125 0.49999988 0.3125 0.51249987 0.3125 0.52499986
		 0.3125 0.53749985 0.3125 0.54999983 0.3125 0.56249982 0.3125 0.57499981 0.3125 0.5874998
		 0.3125 0.59999979 0.3125 0.61249977 0.3125 0.62499976 0.3125 0.375 0.68843985 0.38749999
		 0.68843985 0.39999998 0.68843985 0.41249996 0.68843985 0.42499995 0.68843985 0.43749994
		 0.68843985 0.44999993 0.68843985 0.46249992 0.68843985 0.4749999 0.68843985 0.48749989
		 0.68843985 0.49999988 0.68843985 0.51249987 0.68843985 0.52499986 0.68843985 0.53749985
		 0.68843985 0.54999983 0.68843985 0.56249982 0.68843985 0.57499981 0.68843985 0.5874998
		 0.68843985 0.59999979 0.68843985 0.61249977 0.68843985 0.62499976 0.68843985 0.64860266
		 0.79546607 0.62640899 0.75190848 0.59184152 0.71734101 0.54828393 0.69514734 0.5
		 0.68749994 0.45171607 0.69514734 0.40815851 0.71734107 0.37359107 0.75190854 0.3513974
		 0.79546607 0.34374997 0.84375 0.3513974 0.89203393 0.37359107 0.93559146 0.40815854
		 0.97015893 0.4517161 0.9923526 0.5 1 0.54828387 0.9923526 0.59184146 0.97015893 0.62640893
		 0.93559146 0.6486026 0.89203393 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 20 ".pt[20:39]" -type "float3"  0 0 7.4505806e-09 -2.9802322e-08 
		0 0 -1.4901161e-08 0 -2.9802322e-08 -1.4901161e-08 0 -2.9802322e-08 0 0 -5.9604645e-08 
		7.4505806e-09 0 -2.9802322e-08 -1.4901161e-08 0 -2.9802322e-08 0 0 0 0 0 -7.4505806e-09 
		-2.9802322e-08 0 0 0 0 7.4505806e-09 2.9802322e-08 0 -4.4703484e-08 -1.4901161e-08 
		0 2.9802322e-08 0 0 2.9802322e-08 3.5527137e-15 0 0 0 0 0 1.4901161e-08 0 0 0 0 -4.4703484e-08 
		0 0 0 2.9802322e-08 0 0;
	setAttr -s 40 ".vt[0:39]"  1.66770887 -0.17842032 -0.54187143 1.41863787 -0.17842032 -1.030700684
		 1.030700684 -0.17842032 -1.41863775 0.54187137 -0.17842032 -1.66770864 0 -0.17842032 -1.75353253
		 -0.54187137 -0.17842032 -1.66770852 -1.030700445 -0.17842032 -1.41863751 -1.41863739 -0.17842032 -1.030700326
		 -1.66770816 -0.17842032 -0.54187119 -1.75353217 -0.17842032 0 -1.66770816 -0.17842032 0.54187119
		 -1.41863728 -0.17842032 1.030700207 -1.030700207 -0.17842032 1.41863716 -0.54187119 -0.17842032 1.66770804
		 -5.2259317e-08 -0.17842032 1.75353193 0.54187107 -0.17842032 1.66770792 1.030700088 -0.17842032 1.41863704
		 1.41863692 -0.17842032 1.030700207 1.6677078 -0.17842032 0.54187113 1.75353169 -0.17842032 0
		 1.66770887 0.17842032 -0.54187143 1.41863787 0.17842032 -1.030700684 1.030700684 0.17842032 -1.41863775
		 0.54187137 0.17842032 -1.66770864 0 0.17842032 -1.75353265 -0.54187137 0.17842032 -1.66770852
		 -1.030700445 0.17842032 -1.41863751 -1.41863739 0.17842032 -1.030700326 -1.66770816 0.17842032 -0.54187119
		 -1.75353217 0.17842032 0 -1.66770816 0.17842032 0.54187119 -1.41863728 0.17842032 1.030700207
		 -1.030700207 0.17842032 1.41863716 -0.54187119 0.17842032 1.66770804 -5.2259313e-08 0.17842032 1.75353193
		 0.54187107 0.17842032 1.66770792 1.030700088 0.17842032 1.41863704 1.41863692 0.17842032 1.030700207
		 1.6677078 0.17842032 0.54187113 1.75353169 0.17842032 0;
	setAttr -s 60 ".ed[0:59]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 6 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 12 0 12 13 0 13 14 0 14 15 0 15 16 0 16 17 0 17 18 0
		 18 19 0 19 0 0 20 21 0 21 22 0 22 23 0 23 24 0 24 25 0 25 26 0 26 27 0 27 28 0 28 29 0
		 29 30 0 30 31 0 31 32 0 32 33 0 33 34 0 34 35 0 35 36 0 36 37 0 37 38 0 38 39 0 39 20 0
		 0 20 1 1 21 1 2 22 1 3 23 1 4 24 1 5 25 1 6 26 1 7 27 1 8 28 1 9 29 1 10 30 1 11 31 1
		 12 32 1 13 33 1 14 34 1 15 35 1 16 36 1 17 37 1 18 38 1 19 39 1;
	setAttr -s 22 -ch 120 ".fc[0:21]" -type "polyFaces" 
		f 4 0 41 -21 -41
		mu 0 4 20 21 42 41
		f 4 1 42 -22 -42
		mu 0 4 21 22 43 42
		f 4 2 43 -23 -43
		mu 0 4 22 23 44 43
		f 4 3 44 -24 -44
		mu 0 4 23 24 45 44
		f 4 4 45 -25 -45
		mu 0 4 24 25 46 45
		f 4 5 46 -26 -46
		mu 0 4 25 26 47 46
		f 4 6 47 -27 -47
		mu 0 4 26 27 48 47
		f 4 7 48 -28 -48
		mu 0 4 27 28 49 48
		f 4 8 49 -29 -49
		mu 0 4 28 29 50 49
		f 4 9 50 -30 -50
		mu 0 4 29 30 51 50
		f 4 10 51 -31 -51
		mu 0 4 30 31 52 51
		f 4 11 52 -32 -52
		mu 0 4 31 32 53 52
		f 4 12 53 -33 -53
		mu 0 4 32 33 54 53
		f 4 13 54 -34 -54
		mu 0 4 33 34 55 54
		f 4 14 55 -35 -55
		mu 0 4 34 35 56 55
		f 4 15 56 -36 -56
		mu 0 4 35 36 57 56
		f 4 16 57 -37 -57
		mu 0 4 36 37 58 57
		f 4 17 58 -38 -58
		mu 0 4 37 38 59 58
		f 4 18 59 -39 -59
		mu 0 4 38 39 60 59
		f 4 19 40 -40 -60
		mu 0 4 39 40 61 60
		f 20 -20 -19 -18 -17 -16 -15 -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1
		mu 0 20 0 19 18 17 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
		f 20 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39
		mu 0 20 80 79 78 77 76 75 74 73 72 71 70 69 68 67 66 65 64 63 62 81;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 10 ".lnk";
	setAttr -s 10 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 5;
	setAttr -s 6 ".dli[1:5]"  1 2 3 4 5;
	setAttr -s 4 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode mentalrayItemsList -s -n "mentalrayItemsList";
	setAttr -s 2 ".opt";
createNode mentalrayGlobals -s -n "mentalrayGlobals";
	addAttr -ci true -h true -sn "sunAndSkyShader" -ln "sunAndSkyShader" -at "message";
	setAttr ".rvb" 3;
	setAttr ".ivb" no;
createNode mentalrayOptions -s -n "miDefaultOptions";
	addAttr -ci true -m -sn "stringOptions" -ln "stringOptions" -at "compound" -nc 
		3;
	addAttr -ci true -sn "name" -ln "name" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "value" -ln "value" -dt "string" -p "stringOptions";
	addAttr -ci true -sn "type" -ln "type" -dt "string" -p "stringOptions";
	setAttr -s 45 ".stringOptions";
	setAttr ".stringOptions[0].name" -type "string" "rast motion factor";
	setAttr ".stringOptions[0].value" -type "string" "1.0";
	setAttr ".stringOptions[0].type" -type "string" "scalar";
	setAttr ".stringOptions[1].name" -type "string" "rast transparency depth";
	setAttr ".stringOptions[1].value" -type "string" "8";
	setAttr ".stringOptions[1].type" -type "string" "integer";
	setAttr ".stringOptions[2].name" -type "string" "rast useopacity";
	setAttr ".stringOptions[2].value" -type "string" "true";
	setAttr ".stringOptions[2].type" -type "string" "boolean";
	setAttr ".stringOptions[3].name" -type "string" "importon";
	setAttr ".stringOptions[3].value" -type "string" "false";
	setAttr ".stringOptions[3].type" -type "string" "boolean";
	setAttr ".stringOptions[4].name" -type "string" "importon density";
	setAttr ".stringOptions[4].value" -type "string" "1.0";
	setAttr ".stringOptions[4].type" -type "string" "scalar";
	setAttr ".stringOptions[5].name" -type "string" "importon merge";
	setAttr ".stringOptions[5].value" -type "string" "0.0";
	setAttr ".stringOptions[5].type" -type "string" "scalar";
	setAttr ".stringOptions[6].name" -type "string" "importon trace depth";
	setAttr ".stringOptions[6].value" -type "string" "0";
	setAttr ".stringOptions[6].type" -type "string" "integer";
	setAttr ".stringOptions[7].name" -type "string" "importon traverse";
	setAttr ".stringOptions[7].value" -type "string" "true";
	setAttr ".stringOptions[7].type" -type "string" "boolean";
	setAttr ".stringOptions[8].name" -type "string" "shadowmap pixel samples";
	setAttr ".stringOptions[8].value" -type "string" "3";
	setAttr ".stringOptions[8].type" -type "string" "integer";
	setAttr ".stringOptions[9].name" -type "string" "ambient occlusion";
	setAttr ".stringOptions[9].value" -type "string" "false";
	setAttr ".stringOptions[9].type" -type "string" "boolean";
	setAttr ".stringOptions[10].name" -type "string" "ambient occlusion rays";
	setAttr ".stringOptions[10].value" -type "string" "256";
	setAttr ".stringOptions[10].type" -type "string" "integer";
	setAttr ".stringOptions[11].name" -type "string" "ambient occlusion cache";
	setAttr ".stringOptions[11].value" -type "string" "false";
	setAttr ".stringOptions[11].type" -type "string" "boolean";
	setAttr ".stringOptions[12].name" -type "string" "ambient occlusion cache density";
	setAttr ".stringOptions[12].value" -type "string" "1.0";
	setAttr ".stringOptions[12].type" -type "string" "scalar";
	setAttr ".stringOptions[13].name" -type "string" "ambient occlusion cache points";
	setAttr ".stringOptions[13].value" -type "string" "64";
	setAttr ".stringOptions[13].type" -type "string" "integer";
	setAttr ".stringOptions[14].name" -type "string" "irradiance particles";
	setAttr ".stringOptions[14].value" -type "string" "false";
	setAttr ".stringOptions[14].type" -type "string" "boolean";
	setAttr ".stringOptions[15].name" -type "string" "irradiance particles rays";
	setAttr ".stringOptions[15].value" -type "string" "256";
	setAttr ".stringOptions[15].type" -type "string" "integer";
	setAttr ".stringOptions[16].name" -type "string" "irradiance particles interpolate";
	setAttr ".stringOptions[16].value" -type "string" "1";
	setAttr ".stringOptions[16].type" -type "string" "integer";
	setAttr ".stringOptions[17].name" -type "string" "irradiance particles interppoints";
	setAttr ".stringOptions[17].value" -type "string" "64";
	setAttr ".stringOptions[17].type" -type "string" "integer";
	setAttr ".stringOptions[18].name" -type "string" "irradiance particles indirect passes";
	setAttr ".stringOptions[18].value" -type "string" "0";
	setAttr ".stringOptions[18].type" -type "string" "integer";
	setAttr ".stringOptions[19].name" -type "string" "irradiance particles scale";
	setAttr ".stringOptions[19].value" -type "string" "1.0";
	setAttr ".stringOptions[19].type" -type "string" "scalar";
	setAttr ".stringOptions[20].name" -type "string" "irradiance particles env";
	setAttr ".stringOptions[20].value" -type "string" "true";
	setAttr ".stringOptions[20].type" -type "string" "boolean";
	setAttr ".stringOptions[21].name" -type "string" "irradiance particles env rays";
	setAttr ".stringOptions[21].value" -type "string" "256";
	setAttr ".stringOptions[21].type" -type "string" "integer";
	setAttr ".stringOptions[22].name" -type "string" "irradiance particles env scale";
	setAttr ".stringOptions[22].value" -type "string" "1";
	setAttr ".stringOptions[22].type" -type "string" "integer";
	setAttr ".stringOptions[23].name" -type "string" "irradiance particles rebuild";
	setAttr ".stringOptions[23].value" -type "string" "true";
	setAttr ".stringOptions[23].type" -type "string" "boolean";
	setAttr ".stringOptions[24].name" -type "string" "irradiance particles file";
	setAttr ".stringOptions[24].value" -type "string" "";
	setAttr ".stringOptions[24].type" -type "string" "string";
	setAttr ".stringOptions[25].name" -type "string" "geom displace motion factor";
	setAttr ".stringOptions[25].value" -type "string" "1.0";
	setAttr ".stringOptions[25].type" -type "string" "scalar";
	setAttr ".stringOptions[26].name" -type "string" "contrast all buffers";
	setAttr ".stringOptions[26].value" -type "string" "true";
	setAttr ".stringOptions[26].type" -type "string" "boolean";
	setAttr ".stringOptions[27].name" -type "string" "finalgather normal tolerance";
	setAttr ".stringOptions[27].value" -type "string" "25.842";
	setAttr ".stringOptions[27].type" -type "string" "scalar";
	setAttr ".stringOptions[28].name" -type "string" "trace camera clip";
	setAttr ".stringOptions[28].value" -type "string" "false";
	setAttr ".stringOptions[28].type" -type "string" "boolean";
	setAttr ".stringOptions[29].name" -type "string" "unified sampling";
	setAttr ".stringOptions[29].value" -type "string" "true";
	setAttr ".stringOptions[29].type" -type "string" "boolean";
	setAttr ".stringOptions[30].name" -type "string" "samples quality";
	setAttr ".stringOptions[30].value" -type "string" "0.25 0.25 0.25 0.25";
	setAttr ".stringOptions[30].type" -type "string" "color";
	setAttr ".stringOptions[31].name" -type "string" "samples min";
	setAttr ".stringOptions[31].value" -type "string" "1.0";
	setAttr ".stringOptions[31].type" -type "string" "scalar";
	setAttr ".stringOptions[32].name" -type "string" "samples max";
	setAttr ".stringOptions[32].value" -type "string" "100.0";
	setAttr ".stringOptions[32].type" -type "string" "scalar";
	setAttr ".stringOptions[33].name" -type "string" "samples error cutoff";
	setAttr ".stringOptions[33].value" -type "string" "0.0 0.0 0.0 0.0";
	setAttr ".stringOptions[33].type" -type "string" "color";
	setAttr ".stringOptions[34].name" -type "string" "samples per object";
	setAttr ".stringOptions[34].value" -type "string" "false";
	setAttr ".stringOptions[34].type" -type "string" "boolean";
	setAttr ".stringOptions[35].name" -type "string" "progressive";
	setAttr ".stringOptions[35].value" -type "string" "false";
	setAttr ".stringOptions[35].type" -type "string" "boolean";
	setAttr ".stringOptions[36].name" -type "string" "progressive max time";
	setAttr ".stringOptions[36].value" -type "string" "0";
	setAttr ".stringOptions[36].type" -type "string" "integer";
	setAttr ".stringOptions[37].name" -type "string" "progressive subsampling size";
	setAttr ".stringOptions[37].value" -type "string" "1";
	setAttr ".stringOptions[37].type" -type "string" "integer";
	setAttr ".stringOptions[38].name" -type "string" "iray";
	setAttr ".stringOptions[38].value" -type "string" "false";
	setAttr ".stringOptions[38].type" -type "string" "boolean";
	setAttr ".stringOptions[39].name" -type "string" "light relative scale";
	setAttr ".stringOptions[39].value" -type "string" "0.31831";
	setAttr ".stringOptions[39].type" -type "string" "scalar";
	setAttr ".stringOptions[40].name" -type "string" "trace camera motion vectors";
	setAttr ".stringOptions[40].value" -type "string" "false";
	setAttr ".stringOptions[40].type" -type "string" "boolean";
	setAttr ".stringOptions[41].name" -type "string" "ray differentials";
	setAttr ".stringOptions[41].value" -type "string" "true";
	setAttr ".stringOptions[41].type" -type "string" "boolean";
	setAttr ".stringOptions[42].name" -type "string" "environment lighting mode";
	setAttr ".stringOptions[42].value" -type "string" "off";
	setAttr ".stringOptions[42].type" -type "string" "string";
	setAttr ".stringOptions[43].name" -type "string" "environment lighting quality";
	setAttr ".stringOptions[43].value" -type "string" "0.167";
	setAttr ".stringOptions[43].type" -type "string" "scalar";
	setAttr ".stringOptions[44].name" -type "string" "environment lighting shadow";
	setAttr ".stringOptions[44].value" -type "string" "transparent";
	setAttr ".stringOptions[44].type" -type "string" "string";
createNode mentalrayFramebuffer -s -n "miDefaultFramebuffer";
createNode shadingEngine -n "phong1SG";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode displayLayer -n "outer_case_layer";
	setAttr ".c" 18;
	setAttr ".do" 1;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n"
		+ "                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n"
		+ "                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n"
		+ "            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n"
		+ "                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n"
		+ "                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n"
		+ "                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n"
		+ "            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n"
		+ "            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n"
		+ "                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n"
		+ "                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n"
		+ "                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n"
		+ "            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n"
		+ "            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n"
		+ "        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n"
		+ "                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"base_OpenGL_Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n"
		+ "                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n"
		+ "                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 1\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"base_OpenGL_Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n"
		+ "            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n"
		+ "            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -showShapes 0\n                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n"
		+ "            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n"
		+ "            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n"
		+ "                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n"
		+ "                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n"
		+ "                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n"
		+ "                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n"
		+ "                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n"
		+ "                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n"
		+ "                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n"
		+ "                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n"
		+ "                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n"
		+ "            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -island 0\n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"Stereo\" -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels `;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n"
		+ "                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n"
		+ "                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\nstring $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n"
		+ "                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n"
		+ "                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -fluids 1\n                -hairSystems 1\n"
		+ "                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n"
		+ "\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 1\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 0\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"base_OpenGL_Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode polyBridgeEdge -n "polyBridgeEdge1";
	setAttr ".c[0]"  0 1 1;
	setAttr ".dv" 5;
createNode groupId -n "groupId14";
	setAttr ".ihi" 0;
createNode blinn -n "control_panel_metal";
	setAttr ".c" -type "float3" 0.13725491 0.12648982 0.12648982 ;
	setAttr ".ambc" -type "float3" 0.36841384 0.36841384 0.36841384 ;
createNode shadingEngine -n "blinn1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo2";
createNode lambert -n "joystick";
	setAttr ".c" -type "float3" 0.21568628 0.18608227 0.18608227 ;
createNode shadingEngine -n "lambert2SG";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo3";
createNode lambert -n "button_plastic";
	setAttr ".c" -type "float3" 1 0.078431368 0.078431368 ;
createNode shadingEngine -n "lambert3SG";
	setAttr ".ihi" 0;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo4";
createNode blinn -n "prize_door_plastic";
	setAttr ".c" -type "float3" 1 1 1 ;
createNode shadingEngine -n "blinn2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo5";
createNode phong -n "outer_case_metal";
	setAttr ".c" -type "float3" 0.23100001 0.23100001 0.23100001 ;
	setAttr ".sc" -type "float3" 0.43608758 0.43608758 0.43608758 ;
	setAttr ".rfl" 0.59130436182022095;
createNode shadingEngine -n "phong2SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo6";
createNode mentalrayOptions -s -n "miContourPreset";
createNode displayLayer -n "glass_layer";
	setAttr ".dt" 1;
	setAttr ".c" 21;
	setAttr ".do" 2;
createNode anisotropic -n "anisotropic1";
	setAttr ".c" -type "float3" 0.62591314 0.65129703 0.86274511 ;
	setAttr ".it" -type "float3" 0.68421453 0.68421453 0.68421453 ;
createNode blinn -n "control_arm_metal";
	setAttr ".c" -type "float3" 0.671 0.671 0.671 ;
createNode shadingEngine -n "blinn3SG";
	setAttr ".ihi" 0;
	setAttr -s 4 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo7";
createNode displayLayer -n "internal_layer";
	setAttr ".c" 19;
	setAttr ".do" 3;
createNode blinn -n "claw_mat";
	setAttr ".c" -type "float3" 0.671 0.671 0.671 ;
createNode shadingEngine -n "blinn4SG";
	setAttr ".ihi" 0;
	setAttr -s 3 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo8";
select -ne :time1;
	setAttr ".o" 1;
	setAttr ".unw" 1;
select -ne :renderPartition;
	setAttr -s 10 ".st";
select -ne :initialShadingGroup;
	setAttr -s 2 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultShaderList1;
	setAttr -s 10 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
select -ne :defaultRenderGlobals;
	setAttr ".ren" -type "string" "mentalRay";
	setAttr ".outf" 8;
	setAttr ".imfkey" -type "string" "jpg";
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 18 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surfaces" "Particles" "Fluids" "Image Planes" "UI:" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 18 0 1 1 1 1 1
		 1 0 0 0 0 0 0 0 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "internal_layer.di" "control_arm_stabilizer_right.do";
connectAttr "internal_layer.di" "control_arm_stabilizer_left.do";
connectAttr "internal_layer.di" "crane_prize_base.do";
connectAttr "glass_layer.di" "window_front.do";
connectAttr "glass_layer.di" "window_back.do";
connectAttr "glass_layer.di" "window_right.do";
connectAttr "glass_layer.di" "window_left.do";
connectAttr "outer_case_layer.di" "outer_case.do";
connectAttr "outer_case_layer.di" "prize_door.do";
connectAttr "internal_layer.di" "control_arm_mount.do";
connectAttr "internal_layer.di" "control_arm.do";
connectAttr "internal_layer.di" "control_arm1.do";
connectAttr "outer_case_layer.di" "control_panel_box.do";
connectAttr "outer_case_layer.di" "button.do";
connectAttr "outer_case_layer.di" "button_base.do";
connectAttr "outer_case_layer.di" "joystick_knob.do";
connectAttr "outer_case_layer.di" "joystick_pole.do";
connectAttr "outer_case_layer.di" "joystick_pole_base.do";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong2SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "lambert3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong2SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn3SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "blinn4SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":mentalrayGlobals.msg" ":mentalrayItemsList.glb";
connectAttr ":miDefaultOptions.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miContourPreset.msg" ":mentalrayItemsList.opt" -na;
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayItemsList.fb" -na;
connectAttr ":miDefaultOptions.msg" ":mentalrayGlobals.opt";
connectAttr ":miDefaultFramebuffer.msg" ":mentalrayGlobals.fb";
connectAttr "anisotropic1.oc" "phong1SG.ss";
connectAttr "window_leftShape.iog" "phong1SG.dsm" -na;
connectAttr "window_rightShape.iog" "phong1SG.dsm" -na;
connectAttr "window_backShape.iog" "phong1SG.dsm" -na;
connectAttr "window_frontShape.iog" "phong1SG.dsm" -na;
connectAttr "phong1SG.msg" "materialInfo1.sg";
connectAttr "anisotropic1.msg" "materialInfo1.m";
connectAttr "layerManager.dli[2]" "outer_case_layer.id";
connectAttr "control_panel_metal.oc" "blinn1SG.ss";
connectAttr "control_panel_boxShape.iog" "blinn1SG.dsm" -na;
connectAttr "blinn1SG.msg" "materialInfo2.sg";
connectAttr "control_panel_metal.msg" "materialInfo2.m";
connectAttr "joystick.oc" "lambert2SG.ss";
connectAttr "joystick_pole_baseShape.iog" "lambert2SG.dsm" -na;
connectAttr "joystick_knobShape.iog" "lambert2SG.dsm" -na;
connectAttr "joystick_poleShape.iog" "lambert2SG.dsm" -na;
connectAttr "lambert2SG.msg" "materialInfo3.sg";
connectAttr "joystick.msg" "materialInfo3.m";
connectAttr "button_plastic.oc" "lambert3SG.ss";
connectAttr "button_baseShape.iog" "lambert3SG.dsm" -na;
connectAttr "buttonShape.iog" "lambert3SG.dsm" -na;
connectAttr "lambert3SG.msg" "materialInfo4.sg";
connectAttr "button_plastic.msg" "materialInfo4.m";
connectAttr "prize_door_plastic.oc" "blinn2SG.ss";
connectAttr "prize_doorShape.iog" "blinn2SG.dsm" -na;
connectAttr "blinn2SG.msg" "materialInfo5.sg";
connectAttr "prize_door_plastic.msg" "materialInfo5.m";
connectAttr "outer_case_metal.oc" "phong2SG.ss";
connectAttr "outer_caseShape.iog" "phong2SG.dsm" -na;
connectAttr "phong2SG.msg" "materialInfo6.sg";
connectAttr "outer_case_metal.msg" "materialInfo6.m";
connectAttr "layerManager.dli[3]" "glass_layer.id";
connectAttr "control_arm_metal.oc" "blinn3SG.ss";
connectAttr "control_arm_stabilizer_leftShape.iog" "blinn3SG.dsm" -na;
connectAttr "control_arm_stabilizer_rightShape.iog" "blinn3SG.dsm" -na;
connectAttr "control_armShape.iog" "blinn3SG.dsm" -na;
connectAttr "control_arm1Shape.iog" "blinn3SG.dsm" -na;
connectAttr "blinn3SG.msg" "materialInfo7.sg";
connectAttr "control_arm_metal.msg" "materialInfo7.m";
connectAttr "layerManager.dli[5]" "internal_layer.id";
connectAttr "claw_mat.oc" "blinn4SG.ss";
connectAttr "claw_Shape2.iog" "blinn4SG.dsm" -na;
connectAttr "claw_baseShape.iog" "blinn4SG.dsm" -na;
connectAttr "claw_Shape1.iog" "blinn4SG.dsm" -na;
connectAttr "blinn4SG.msg" "materialInfo8.sg";
connectAttr "claw_mat.msg" "materialInfo8.m";
connectAttr "phong1SG.pa" ":renderPartition.st" -na;
connectAttr "blinn1SG.pa" ":renderPartition.st" -na;
connectAttr "lambert2SG.pa" ":renderPartition.st" -na;
connectAttr "lambert3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn2SG.pa" ":renderPartition.st" -na;
connectAttr "phong2SG.pa" ":renderPartition.st" -na;
connectAttr "blinn3SG.pa" ":renderPartition.st" -na;
connectAttr "blinn4SG.pa" ":renderPartition.st" -na;
connectAttr "crane_prize_baseShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "control_arm_mountShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "anisotropic1.msg" ":defaultShaderList1.s" -na;
connectAttr "control_panel_metal.msg" ":defaultShaderList1.s" -na;
connectAttr "joystick.msg" ":defaultShaderList1.s" -na;
connectAttr "button_plastic.msg" ":defaultShaderList1.s" -na;
connectAttr "prize_door_plastic.msg" ":defaultShaderList1.s" -na;
connectAttr "outer_case_metal.msg" ":defaultShaderList1.s" -na;
connectAttr "control_arm_metal.msg" ":defaultShaderList1.s" -na;
connectAttr "claw_mat.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of crane_game.ma
